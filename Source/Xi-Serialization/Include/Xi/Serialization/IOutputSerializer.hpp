/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string_view>
#include <cinttypes>

#include <Xi/Global.hh>
#include <Xi/Byte.hh>

#include "Xi/Serialization/TypeTag.hpp"

namespace Xi {
namespace Serialization {
class IOutputSerializer {
 public:
  virtual ~IOutputSerializer() = default;

  [[nodiscard]] virtual bool writeInt8(std::int8_t value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool writeUInt8(std::uint8_t value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool writeInt16(std::int16_t value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool writeUInt16(std::uint16_t value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool writeInt32(std::int32_t value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool writeUInt32(std::uint32_t value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool writeInt64(std::int64_t value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool writeUInt64(std::uint64_t value, const std::string_view name = "") = 0;

  [[nodiscard]] virtual bool writeBoolean(bool value, const std::string_view name = "") = 0;

  [[nodiscard]] virtual bool writeFloat(float value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool writeDouble(double value, const std::string_view name = "") = 0;

  [[nodiscard]] virtual bool writeString(const std::string_view value,
                                         const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool writeBinary(ConstByteSpan value, const std::string_view name = "") = 0;

  [[nodiscard]] virtual bool writeBlob(ConstByteSpan value, const std::string_view name = "") = 0;

  [[nodiscard]] virtual bool writeTypeTag(const TypeTag& value,
                                          const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool writeFlag(const TypeTagVector& tag,
                                       const std::string_view name = "") = 0;

  [[nodiscard]] virtual bool beginWriteComplex(const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool endWriteComplex() = 0;

  [[nodiscard]] virtual bool beginWriteVector(size_t size, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool endWriteVector() = 0;

  [[nodiscard]] virtual bool beginWriteArray(size_t size, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool endWriteArray() = 0;

  [[nodiscard]] virtual bool writeNull(const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool writeNotNull(const std::string_view name = "") = 0;
};

XI_DECLARE_SMART_POINTER(IOutputSerializer)

}  // namespace Serialization
}  // namespace Xi
