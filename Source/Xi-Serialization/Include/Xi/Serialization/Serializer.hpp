/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string_view>
#include <cinttypes>
#include <type_traits>

#include "Xi/Serialization/IInputSerializer.hpp"
#include "Xi/Serialization/IOutputSerializer.hpp"

///
/// For custom serialization provide a method 'void serialize(Serializer&, TYPE&, const
/// std::string_view)' For variant provide the binary and string tag using
/// XI_DECLARE_TEXT_SERIALIZATION_TAG or XI_DECLARE_BINARY_SERIALIZATION_TAG .
///

namespace Xi {
namespace Serialization {
class Serializer final {
 public:
  enum struct Mode { Input, Output };

 public:
  explicit Serializer(IInputSerializer &ser);
  explicit Serializer(IOutputSerializer &ser);
  ~Serializer();

  Mode mode() const;
  bool isInputMode() const;
  bool isOutputMode() const;

  [[nodiscard]] bool primitive(std::int8_t &value, const std::string_view name = "");
  [[nodiscard]] bool primitive(std::uint8_t &value, const std::string_view name = "");
  [[nodiscard]] bool primitive(std::int16_t &value, const std::string_view name = "");
  [[nodiscard]] bool primitive(std::uint16_t &value, const std::string_view name = "");
  [[nodiscard]] bool primitive(std::int32_t &value, const std::string_view name = "");
  [[nodiscard]] bool primitive(std::uint32_t &value, const std::string_view name = "");
  [[nodiscard]] bool primitive(std::int64_t &value, const std::string_view name = "");
  [[nodiscard]] bool primitive(std::uint64_t &value, const std::string_view name = "");

  [[nodiscard]] bool primitive(bool &value, const std::string_view name = "");

  [[nodiscard]] bool primitive(float &value, const std::string_view name = "");
  [[nodiscard]] bool primitive(double &value, const std::string_view name = "");

  [[nodiscard]] bool string(std::string &value, const std::string_view name = "");
  [[nodiscard]] bool binary(ByteVector &value, const std::string_view name = "");

  [[nodiscard]] bool blob(ByteSpan value, const std::string_view name = "");

  [[nodiscard]] bool maybe(bool &value, const std::string_view name = "");
  [[nodiscard]] bool typeTag(TypeTag &tag, const std::string_view name = "");
  [[nodiscard]] bool flag(TypeTagVector &tag, const std::string_view name = "");

  [[nodiscard]] bool beginComplex(const std::string_view name = "");
  [[nodiscard]] bool endComplex();

  [[nodiscard]] bool beginArray(size_t count, const std::string_view name = "");
  [[nodiscard]] bool endArray();

  [[nodiscard]] bool beginVector(size_t &count, const std::string_view name = "");
  [[nodiscard]] bool endVector();

  template <typename _ValueT>
  [[nodiscard]] bool operator()(_ValueT &value, const std::string_view name = "") {
#define XI_SERIALIZER_FORWARD_CASE(TYPE, METHOD) \
  if constexpr (std::is_same_v<TYPE, _ValueT>)   \
    return this->METHOD(value, name);            \
  else
#define XI_SERIALIZER_PRIMITIVE_FORWARD_CASE(TYPE) XI_SERIALIZER_FORWARD_CASE(TYPE, primitive)

    XI_SERIALIZER_PRIMITIVE_FORWARD_CASE(std::int8_t)
    XI_SERIALIZER_PRIMITIVE_FORWARD_CASE(std::uint8_t)
    XI_SERIALIZER_PRIMITIVE_FORWARD_CASE(std::int16_t)
    XI_SERIALIZER_PRIMITIVE_FORWARD_CASE(std::uint16_t)
    XI_SERIALIZER_PRIMITIVE_FORWARD_CASE(std::int32_t)
    XI_SERIALIZER_PRIMITIVE_FORWARD_CASE(std::uint32_t)
    XI_SERIALIZER_PRIMITIVE_FORWARD_CASE(std::int64_t)
    XI_SERIALIZER_PRIMITIVE_FORWARD_CASE(std::uint64_t)
    XI_SERIALIZER_PRIMITIVE_FORWARD_CASE(bool)
    XI_SERIALIZER_PRIMITIVE_FORWARD_CASE(float)
    XI_SERIALIZER_PRIMITIVE_FORWARD_CASE(double)
    XI_SERIALIZER_FORWARD_CASE(std::string, string)
    return serialize(value, name, *this);

#undef XI_SERIALIZER_PRIMITIVE_FORWARD_CASE
#undef XI_SERIALIZER_FORWARD_CASE
  }

 private:
  struct _Impl;
  std::unique_ptr<_Impl> m_impl;
};

template <typename _ComplexT>
[[nodiscard]] std::enable_if_t<std::is_compound_v<_ComplexT>, bool> serialize(
    _ComplexT &value, Serializer &serializer) {
  XI_RETURN_EC_IF_NOT(value.serialize(serializer), false);
  return true;
}

template <typename _ComplexT>
[[nodiscard]] std::enable_if_t<std::is_compound_v<_ComplexT>, bool> serialize(
    _ComplexT &value, std::string_view name, Serializer &serializer) {
  XI_RETURN_EC_IF_NOT(serializer.beginComplex(name), false);
  XI_RETURN_EC_IF_NOT(serialize(value, serializer), false);
  XI_RETURN_EC_IF_NOT(serializer.endComplex(), false);
  return true;
}

template <typename _ValueT>[[nodiscard]] bool serialize(_ValueT &value, IInputSerializer &input) {
  Serializer serializer{input};
  return serializer(value, "");
}

template <typename _ValueT>[[nodiscard]] bool serialize(_ValueT &value, IOutputSerializer &input) {
  Serializer serializer{input};
  return serializer(value, "");
}

}  // namespace Serialization
}  // namespace Xi
