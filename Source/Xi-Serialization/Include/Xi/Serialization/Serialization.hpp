/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include "Xi/Serialization/Serializer.hpp"
#include "Xi/Serialization/ArraySerialization.hpp"
#include "Xi/Serialization/EnumSerialization.hpp"
#include "Xi/Serialization/VariantSerialization.hpp"
#include "Xi/Serialization/OptionalSerialization.hpp"
#include "Xi/Serialization/BlobSerialization.hpp"

#define XI_SERIALIZATION_COMPLEX_BEGIN \
  [[nodiscard]] bool serialize(::Xi::Serialization::Serializer& serializer) {
#define XI_SERIALIZATION_BASE(CLASS)       \
  if (!this->CLASS::serialize(serializer)) \
    return false;
#define XI_SERIALIZATION_MEMBER(MEMBER, NAME) \
  if (!serializer(MEMBER, NAME))              \
    return false;
#define XI_SERIALIZATION_COMPLEX_END \
  return true;                       \
  }
