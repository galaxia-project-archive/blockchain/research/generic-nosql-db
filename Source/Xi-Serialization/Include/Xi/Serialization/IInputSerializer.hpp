/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string_view>
#include <cinttypes>

#include <Xi/Global.hh>
#include <Xi/Byte.hh>

#include "Xi/Serialization/TypeTag.hpp"

namespace Xi {
namespace Serialization {
class IInputSerializer {
 public:
  virtual ~IInputSerializer() = default;

  [[nodiscard]] virtual bool readInt8(std::int8_t& value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool readUInt8(std::uint8_t& value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool readInt16(std::int16_t& value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool readUInt16(std::uint16_t& value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool readInt32(std::int32_t& value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool readUInt32(std::uint32_t& value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool readInt64(std::int64_t& value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool readUInt64(std::uint64_t& value, const std::string_view name = "") = 0;

  [[nodiscard]] virtual bool readBoolean(bool& value, const std::string_view name = "") = 0;

  [[nodiscard]] virtual bool readFloat(float& value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool readDouble(double& value, const std::string_view name = "") = 0;

  [[nodiscard]] virtual bool readString(std::string& string, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool readBinary(ByteVector& blob, const std::string_view name = "") = 0;

  [[nodiscard]] virtual bool readBlob(ByteSpan out, const std::string_view name = "") = 0;

  [[nodiscard]] virtual bool readTypeTag(TypeTag& value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool readFlag(TypeTagVector& value, const std::string_view name = "") = 0;

  [[nodiscard]] virtual bool beginReadComplex(const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool endReadComplex() = 0;

  [[nodiscard]] virtual bool beginReadVector(size_t& size, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool endReadVector() = 0;

  [[nodiscard]] virtual bool beginReadArray(size_t size, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool endReadArray() = 0;

  [[nodiscard]] virtual bool checkNull(bool& isNull, const std::string_view name = "") = 0;
};

XI_DECLARE_SMART_POINTER(IInputSerializer)

}  // namespace Serialization
}  // namespace Xi
