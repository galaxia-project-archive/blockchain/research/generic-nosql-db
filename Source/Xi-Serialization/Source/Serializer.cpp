/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Serialization/Serializer.hpp"

#include <cstring>

namespace {
class ISerializerConcept {
 public:
  virtual ~ISerializerConcept() = default;

  virtual Xi::Serialization::Serializer::Mode mode() const = 0;

  [[nodiscard]] virtual bool primitive(std::int8_t &value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool primitive(std::uint8_t &value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool primitive(std::int16_t &value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool primitive(std::uint16_t &value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool primitive(std::int32_t &value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool primitive(std::uint32_t &value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool primitive(std::int64_t &value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool primitive(std::uint64_t &value, const std::string_view name = "") = 0;

  [[nodiscard]] virtual bool primitive(bool &value, const std::string_view name = "") = 0;

  [[nodiscard]] virtual bool primitive(float &value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool primitive(double &value, const std::string_view name = "") = 0;

  [[nodiscard]] virtual bool string(std::string &value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool binary(Xi::ByteVector &value, const std::string_view name = "") = 0;

  [[nodiscard]] virtual bool blob(Xi::ByteSpan value, const std::string_view name = "") = 0;

  [[nodiscard]] virtual bool beginComplex(const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool endComplex() = 0;

  [[nodiscard]] virtual bool maybe(bool &value, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool typeTag(Xi::Serialization::TypeTag &value,
                                     const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool flag(Xi::Serialization::TypeTagVector &tag,
                                  const std::string_view name = "") = 0;

  [[nodiscard]] virtual bool beginVector(size_t &count, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool endVector() = 0;

  [[nodiscard]] virtual bool beginArray(size_t count, const std::string_view name = "") = 0;
  [[nodiscard]] virtual bool endArray() = 0;
};

class InputSerializer final : public ISerializerConcept {
  Xi::Serialization::IInputSerializer &input;

 public:
  InputSerializer(Xi::Serialization::IInputSerializer &in) : input{in} {
  }
  ~InputSerializer() override = default;

  Xi::Serialization::Serializer::Mode mode() const override {
    return Xi::Serialization::Serializer::Mode::Input;
  }

  [[nodiscard]] bool primitive(std::int8_t &value, const std::string_view name) override {
    return input.readInt8(value, name);
  }
  [[nodiscard]] bool primitive(std::uint8_t &value, const std::string_view name) override {
    return input.readUInt8(value, name);
  }
  [[nodiscard]] bool primitive(std::int16_t &value, const std::string_view name) override {
    return input.readInt16(value, name);
  }
  [[nodiscard]] bool primitive(std::uint16_t &value, const std::string_view name) override {
    return input.readUInt16(value, name);
  }
  [[nodiscard]] bool primitive(std::int32_t &value, const std::string_view name) override {
    return input.readInt32(value, name);
  }
  [[nodiscard]] bool primitive(std::uint32_t &value, const std::string_view name) override {
    return input.readUInt32(value, name);
  }
  [[nodiscard]] bool primitive(std::int64_t &value, const std::string_view name) override {
    return input.readInt64(value, name);
  }
  [[nodiscard]] bool primitive(std::uint64_t &value, const std::string_view name) override {
    return input.readUInt64(value, name);
  }

  [[nodiscard]] bool primitive(bool &value, const std::string_view name) override {
    return input.readBoolean(value, name);
  }

  [[nodiscard]] bool primitive(float &value, const std::string_view name) override {
    return input.readFloat(value, name);
  }
  [[nodiscard]] bool primitive(double &value, const std::string_view name) override {
    return input.readDouble(value, name);
  }

  [[nodiscard]] bool string(std::string &value, const std::string_view name) override {
    return input.readString(value, name);
  }
  [[nodiscard]] bool binary(Xi::ByteVector &value, const std::string_view name) override {
    return input.readBinary(value, name);
  }

  [[nodiscard]] bool blob(Xi::ByteSpan value, const std::string_view name = "") override {
    return input.readBlob(value, name);
  }

  [[nodiscard]] bool maybe(bool &value, const std::string_view name) override {
    return input.checkNull(value, name);
  }
  [[nodiscard]] bool typeTag(Xi::Serialization::TypeTag &value,
                             const std::string_view name) override {
    return input.readTypeTag(value, name);
  }
  [[nodiscard]] bool flag(Xi::Serialization::TypeTagVector &tag,
                          const std::string_view name) override {
    return input.readFlag(tag, name);
  }

  [[nodiscard]] bool beginComplex(const std::string_view name) override {
    return input.beginReadComplex(name);
  }
  [[nodiscard]] bool endComplex() override {
    return input.endReadComplex();
  }

  [[nodiscard]] bool beginVector(size_t &count, const std::string_view name) override {
    return input.beginReadVector(count, name);
  }
  [[nodiscard]] bool endVector() override {
    return input.endReadVector();
  }

  [[nodiscard]] bool beginArray(size_t count, const std::string_view name) override {
    return input.beginReadArray(count, name);
  }
  [[nodiscard]] bool endArray() override {
    return input.endReadArray();
  }
};

class OutputSerializer final : public ISerializerConcept {
  Xi::Serialization::IOutputSerializer &output;

 public:
  OutputSerializer(Xi::Serialization::IOutputSerializer &out) : output{out} {
  }
  ~OutputSerializer() override = default;

  Xi::Serialization::Serializer::Mode mode() const override {
    return Xi::Serialization::Serializer::Mode::Output;
  }

  [[nodiscard]] bool primitive(std::int8_t &value, const std::string_view name) override {
    return output.writeInt8(value, name);
  }
  [[nodiscard]] bool primitive(std::uint8_t &value, const std::string_view name) override {
    return output.writeUInt8(value, name);
  }
  [[nodiscard]] bool primitive(std::int16_t &value, const std::string_view name) override {
    return output.writeInt16(value, name);
  }
  [[nodiscard]] bool primitive(std::uint16_t &value, const std::string_view name) override {
    return output.writeUInt16(value, name);
  }
  [[nodiscard]] bool primitive(std::int32_t &value, const std::string_view name) override {
    return output.writeInt32(value, name);
  }
  [[nodiscard]] bool primitive(std::uint32_t &value, const std::string_view name) override {
    return output.writeUInt32(value, name);
  }
  [[nodiscard]] bool primitive(std::int64_t &value, const std::string_view name) override {
    return output.writeInt64(value, name);
  }
  [[nodiscard]] bool primitive(std::uint64_t &value, const std::string_view name) override {
    return output.writeUInt64(value, name);
  }

  [[nodiscard]] bool primitive(bool &value, const std::string_view name) override {
    return output.writeBoolean(value, name);
  }

  [[nodiscard]] bool primitive(float &value, const std::string_view name) override {
    return output.writeFloat(value, name);
  }
  [[nodiscard]] bool primitive(double &value, const std::string_view name) override {
    return output.writeDouble(value, name);
  }

  [[nodiscard]] bool string(std::string &value, const std::string_view name) override {
    return output.writeString(value, name);
  }
  [[nodiscard]] bool binary(Xi::ByteVector &value, const std::string_view name) override {
    return output.writeBinary(value, name);
  }

  [[nodiscard]] bool blob(Xi::ByteSpan value, const std::string_view name = "") override {
    return output.writeBlob(value, name);
  }

  [[nodiscard]] bool maybe(bool &value, const std::string_view name = "") override {
    if (value) {
      return output.writeNotNull(name);
    } else {
      return output.writeNull(name);
    }
  }

  [[nodiscard]] bool typeTag(Xi::Serialization::TypeTag &value,
                             const std::string_view name) override {
    return output.writeTypeTag(value, name);
  }

  [[nodiscard]] bool flag(Xi::Serialization::TypeTagVector &tag,
                          const std::string_view name) override {
    return output.writeFlag(tag, name);
  }

  [[nodiscard]] bool beginComplex(const std::string_view name) override {
    return output.beginWriteComplex(name);
  }
  [[nodiscard]] bool endComplex() override {
    return output.endWriteComplex();
  }

  [[nodiscard]] bool beginVector(size_t &count, const std::string_view name) override {
    return output.beginWriteVector(count, name);
  }
  [[nodiscard]] bool endVector() override {
    return output.endWriteVector();
  }

  [[nodiscard]] bool beginArray(size_t count, const std::string_view name) override {
    return output.beginWriteArray(count, name);
  }
  [[nodiscard]] bool endArray() override {
    return output.endWriteArray();
  }
};
}  // namespace

struct Xi::Serialization::Serializer::_Impl {
  std::unique_ptr<ISerializerConcept> inner;

  explicit _Impl(IInputSerializer &input) : inner{new InputSerializer{input}} {
  }
  explicit _Impl(IOutputSerializer &output) : inner{new OutputSerializer{output}} {
  }
};

Xi::Serialization::Serializer::Serializer(Xi::Serialization::IInputSerializer &ser)
    : m_impl{new _Impl{ser}} {
}

Xi::Serialization::Serializer::Serializer(Xi::Serialization::IOutputSerializer &ser)
    : m_impl{new _Impl{ser}} {
}

Xi::Serialization::Serializer::~Serializer() {
}

Xi::Serialization::Serializer::Mode Xi::Serialization::Serializer::mode() const {
  return m_impl->inner->mode();
}

bool Xi::Serialization::Serializer::isInputMode() const {
  return mode() == Mode::Input;
}

bool Xi::Serialization::Serializer::isOutputMode() const {
  return mode() == Mode::Output;
}

bool Xi::Serialization::Serializer::primitive(int8_t &value, const std::string_view name) {
  return m_impl->inner->primitive(value, name);
}

bool Xi::Serialization::Serializer::primitive(uint8_t &value, const std::string_view name) {
  return m_impl->inner->primitive(value, name);
}

bool Xi::Serialization::Serializer::primitive(int16_t &value, const std::string_view name) {
  return m_impl->inner->primitive(value, name);
}

bool Xi::Serialization::Serializer::primitive(uint16_t &value, const std::string_view name) {
  return m_impl->inner->primitive(value, name);
}

bool Xi::Serialization::Serializer::primitive(int32_t &value, const std::string_view name) {
  return m_impl->inner->primitive(value, name);
}

bool Xi::Serialization::Serializer::primitive(uint32_t &value, const std::string_view name) {
  return m_impl->inner->primitive(value, name);
}

bool Xi::Serialization::Serializer::primitive(int64_t &value, const std::string_view name) {
  return m_impl->inner->primitive(value, name);
}

bool Xi::Serialization::Serializer::primitive(uint64_t &value, const std::string_view name) {
  return m_impl->inner->primitive(value, name);
}

bool Xi::Serialization::Serializer::primitive(bool &value, const std::string_view name) {
  return m_impl->inner->primitive(value, name);
}

bool Xi::Serialization::Serializer::primitive(float &value, const std::string_view name) {
  return m_impl->inner->primitive(value, name);
}

bool Xi::Serialization::Serializer::primitive(double &value, const std::string_view name) {
  return m_impl->inner->primitive(value, name);
}

bool Xi::Serialization::Serializer::string(std::string &value, const std::string_view name) {
  return m_impl->inner->string(value, name);
}

bool Xi::Serialization::Serializer::binary(Xi::ByteVector &value, const std::string_view name) {
  return m_impl->inner->binary(value, name);
}

bool Xi::Serialization::Serializer::blob(ByteSpan value, const std::string_view name) {
  return m_impl->inner->blob(value, name);
}

bool Xi::Serialization::Serializer::maybe(bool &value, const std::string_view name) {
  return m_impl->inner->maybe(value, name);
}

bool Xi::Serialization::Serializer::typeTag(Xi::Serialization::TypeTag &tag,
                                            const std::string_view name) {
  return m_impl->inner->typeTag(tag, name);
}

bool Xi::Serialization::Serializer::flag(Xi::Serialization::TypeTagVector &tag,
                                         const std::string_view name) {
  return m_impl->inner->flag(tag, name);
}

bool Xi::Serialization::Serializer::beginComplex(const std::string_view name) {
  return m_impl->inner->beginComplex(name);
}

bool Xi::Serialization::Serializer::endComplex() {
  return m_impl->inner->endComplex();
}

bool Xi::Serialization::Serializer::beginArray(size_t count, const std::string_view name) {
  return m_impl->inner->beginArray(count, name);
}

bool Xi::Serialization::Serializer::endArray() {
  return m_impl->inner->endArray();
}

bool Xi::Serialization::Serializer::beginVector(size_t &count, const std::string_view name) {
  return m_impl->inner->beginVector(count, name);
}

bool Xi::Serialization::Serializer::endVector() {
  return m_impl->inner->endArray();
}
