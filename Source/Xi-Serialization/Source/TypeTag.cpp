/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Serialization/TypeTag.hpp"

#include <utility>
#include <limits>
#include <cinttypes>

const Xi::Serialization::TypeTag Xi::Serialization::TypeTag::Null{NoBinaryTag, NoTextTag};

const Xi::Serialization::TypeTag::binary_type Xi::Serialization::TypeTag::NoBinaryTag{
    std::numeric_limits<Xi::Serialization::TypeTag::binary_type>::min()};

const Xi::Serialization::TypeTag::text_type Xi::Serialization::TypeTag::NoTextTag{""};

Xi::Serialization::TypeTag::TypeTag(binary_type binary, text_type text)
    : m_binary{binary}, m_text{std::move(text)} {
}

Xi::Serialization::TypeTag::binary_type Xi::Serialization::TypeTag::binary() const {
  return m_binary;
}

const Xi::Serialization::TypeTag::text_type &Xi::Serialization::TypeTag::text() const {
  return m_text;
}

bool Xi::Serialization::TypeTag::isNull() const {
  return binary() == NoBinaryTag && text() == NoTextTag;
}

bool Xi::Serialization::TypeTag::operator==(const Xi::Serialization::TypeTag &rhs) const {
  if (binary() != NoBinaryTag && binary() == rhs.binary()) {
    return true;
  } else if (text() != NoTextTag && text() == rhs.text()) {
    return true;
  } else {
    return false;
  }
}

bool Xi::Serialization::TypeTag::operator!=(const Xi::Serialization::TypeTag &rhs) const {
  return !(*this == rhs);
}
