/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Stream/InMemoryStreams.hpp>
#include <Xi/Serialization/Serializer.hpp>
#include <Xi/Serialization/Json/OutputSerializer.hpp>
#include <Xi/Serialization/Json/InputSerializer.hpp>
#include <Xi/Serialization/Binary/OutputSerializer.hpp>
#include <Xi/Serialization/Binary/InputSerializer.hpp>

#define XI_TEST_SUITE Xi_Serialization_Serializer

namespace {
template <typename _InputSearializer, typename _OutputSerializer> void genericTest() {
  using namespace Xi::Serialization;

  Xi::ByteVector buffer;

  auto ostream = Xi::Stream::asOutputStream(buffer);
  _OutputSerializer oNativeSerializer{*ostream};
  Serializer oSerializer{oNativeSerializer};

  uint64_t outerArraySize = 5;
  uint64_t innerArraySize = 20;
  ASSERT_TRUE(oSerializer.beginVector(outerArraySize));
  for (size_t i = 0; i < outerArraySize; ++i) {
    ASSERT_TRUE(oSerializer.beginComplex(""));
    ASSERT_TRUE(oSerializer.beginVector(innerArraySize, "numbers"));
    for (size_t j = 0; j < innerArraySize; ++j) {
      uint64_t jNum = i * j;
      ASSERT_TRUE(oSerializer(jNum, ""));
    }
    ASSERT_TRUE(oSerializer.endVector());
    std::string meow{"Meow!"};
    ASSERT_TRUE(oSerializer(meow, "come cats"));
    ASSERT_TRUE(oSerializer.endComplex());
  }
  ASSERT_TRUE(oSerializer.endVector());

  auto istream = Xi::Stream::asInputStream(buffer);
  _InputSearializer iNativeSerializer{*istream};
  Serializer iSerializer{iNativeSerializer};

  uint64_t _outerArraySize = 0;
  ASSERT_TRUE(iSerializer.beginVector(_outerArraySize));
  EXPECT_EQ(outerArraySize, _outerArraySize);
  for (size_t i = 0; i < outerArraySize; ++i) {
    ASSERT_TRUE(iSerializer.beginComplex(""));
    uint64_t _innerArraySize = 0;
    ASSERT_TRUE(iSerializer.beginVector(_innerArraySize, "numbers"));
    EXPECT_EQ(innerArraySize, _innerArraySize);
    for (size_t j = 0; j < innerArraySize; ++j) {
      uint64_t jNum = 0;
      ASSERT_TRUE(iSerializer(jNum, ""));
      EXPECT_EQ(jNum, i * j);
    }
    ASSERT_TRUE(iSerializer.endVector());
    std::string meow{};
    ASSERT_TRUE(iSerializer(meow, "come cats"));
    EXPECT_EQ(meow, "Meow!");
    ASSERT_TRUE(iSerializer.endComplex());
  }
  ASSERT_TRUE(iSerializer.endVector());
}
}  // namespace

TEST(XI_TEST_SUITE, Json) {
  using namespace Xi::Serialization::Json;
  genericTest<InputSerializer, OutputSerializer>();
}

TEST(XI_TEST_SUITE, Binary) {
  using namespace Xi::Serialization::Binary;
  genericTest<InputSerializer, OutputSerializer>();
}
