/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Serialization/Binary/OutputSerializer.hpp"

#include <limits>

#include <Xi/Global.hh>
#include <Xi/Endianess/Endianess.hh>
#include <Xi/Encoding/VarInt.hh>

namespace {
template <typename _IntT>
[[nodiscard]] bool writeVarInt(_IntT value, Xi::Stream::IOutputStream &stream) {
  Xi::ByteArray<Xi::Encoding::VarInt::maximumEncodingSize<_IntT>()> buffer;
  const auto encodingSize = Xi::Encoding::VarInt::encode(value, buffer);
  XI_RETURN_EC_IF(encodingSize.isError(), false);
  XI_RETURN_EC_IF_NOT(
      Xi::Stream::writeStrict(stream, Xi::ConstByteSpan{buffer.data(), *encodingSize}), false);
  return true;
}
template <typename _IntT>
[[nodiscard]] bool writeInt(_IntT value, Xi::Stream::IOutputStream &stream,
                            const Xi::Serialization::Binary::Options &options) {
  value = Xi::Endianess::convert(value, options.endianess);
  XI_RETURN_EC_IF_NOT(Xi::Stream::writeStrict(stream, Xi::asByteSpan(&value, sizeof(_IntT))),
                      false);
  return true;
}
}  // namespace

Xi::Serialization::Binary::OutputSerializer::OutputSerializer(Xi::Stream::IOutputStream &stream,
                                                              Options options)
    : m_stream{stream}, m_options{options} {
}

bool Xi::Serialization::Binary::OutputSerializer::writeInt8(int8_t value,
                                                            const std::string_view name) {
  XI_UNUSED(name);
  return m_stream.write(ByteSpan{reinterpret_cast<Byte *>(&value), 1}) > 0;
}

bool Xi::Serialization::Binary::OutputSerializer::writeUInt8(uint8_t value,
                                                             const std::string_view name) {
  XI_UNUSED(name);
  return m_stream.write(ByteSpan{reinterpret_cast<Byte *>(&value), 1}) > 0;
}

bool Xi::Serialization::Binary::OutputSerializer::writeInt16(int16_t value,
                                                             const std::string_view name) {
  XI_UNUSED(name);
  return hasFlag(m_options.varInt, VarIntUsage::Int16) ? writeVarInt(value, m_stream)
                                                       : writeInt(value, m_stream, m_options);
}

bool Xi::Serialization::Binary::OutputSerializer::writeUInt16(uint16_t value,
                                                              const std::string_view name) {
  XI_UNUSED(name);
  return hasFlag(m_options.varInt, VarIntUsage::UInt16) ? writeVarInt(value, m_stream)
                                                        : writeInt(value, m_stream, m_options);
}

bool Xi::Serialization::Binary::OutputSerializer::writeInt32(int32_t value,
                                                             const std::string_view name) {
  XI_UNUSED(name);
  return hasFlag(m_options.varInt, VarIntUsage::Int32) ? writeVarInt(value, m_stream)
                                                       : writeInt(value, m_stream, m_options);
}

bool Xi::Serialization::Binary::OutputSerializer::writeUInt32(uint32_t value,
                                                              const std::string_view name) {
  XI_UNUSED(name);
  return hasFlag(m_options.varInt, VarIntUsage::UInt32) ? writeVarInt(value, m_stream)
                                                        : writeInt(value, m_stream, m_options);
}

bool Xi::Serialization::Binary::OutputSerializer::writeInt64(int64_t value,
                                                             const std::string_view name) {
  XI_UNUSED(name);
  return hasFlag(m_options.varInt, VarIntUsage::Int64) ? writeVarInt(value, m_stream)
                                                       : writeInt(value, m_stream, m_options);
}

bool Xi::Serialization::Binary::OutputSerializer::writeUInt64(uint64_t value,
                                                              const std::string_view name) {
  XI_UNUSED(name);
  return hasFlag(m_options.varInt, VarIntUsage::UInt64) ? writeVarInt(value, m_stream)
                                                        : writeInt(value, m_stream, m_options);
}

bool Xi::Serialization::Binary::OutputSerializer::writeBoolean(bool value,
                                                               const std::string_view name) {
  XI_UNUSED(name);
  uint8_t nativeValue = value ? 0b01010101 : 0b10101010;
  return writeUInt8(nativeValue, name);
}

bool Xi::Serialization::Binary::OutputSerializer::writeFloat(float value,
                                                             const std::string_view name) {
  static_assert(std::numeric_limits<float>::is_iec559,
                "binary floating serialization is not supported on this architecture");
  union {
    float asFloating;
    ByteArray<sizeof(float)> asBlob;
  } data;
  data.asFloating = value;
  return writeBlob(data.asBlob, name);
}

bool Xi::Serialization::Binary::OutputSerializer::writeDouble(double value,
                                                              const std::string_view name) {
  static_assert(std::numeric_limits<double>::is_iec559,
                "binary floating serialization is not supported on this architecture");
  union {
    double asFloating;
    ByteArray<sizeof(double)> asUInt;
  } data;
  data.asFloating = value;
  return writeBlob(data.asUInt, name);
}

bool Xi::Serialization::Binary::OutputSerializer::writeTypeTag(
    const Xi::Serialization::TypeTag &value, const std::string_view name) {
  XI_RETURN_EC_IF(value.binary() == TypeTag::NoBinaryTag, false);
  return writeUInt64(value.binary(), name);
}

bool Xi::Serialization::Binary::OutputSerializer::writeFlag(
    const Xi::Serialization::TypeTagVector &flag, const std::string_view name) {
  if (flag.empty()) {
    return writeNull(name);
  } else {
    size_t count = flag.size();
    XI_RETURN_EC_IF(count > TypeTag::maximumFlags(), false);
    XI_RETURN_EC_IF_NOT(beginWriteVector(count, name), false);
    for (size_t i = 0; i < count; ++i) {
      XI_RETURN_EC_IF_NOT(writeTypeTag(flag[i], ""), false);
    }
    return endWriteVector();
  }
}

bool Xi::Serialization::Binary::OutputSerializer::writeString(const std::string_view value,
                                                              const std::string_view name) {
  XI_RETURN_EC_IF_NOT(writeUInt64(value.size(), name), false);
  return writeBlob(asByteSpan(value), name);
}

bool Xi::Serialization::Binary::OutputSerializer::writeBinary(ConstByteSpan value,
                                                              const std::string_view name) {
  XI_RETURN_EC_IF_NOT(writeUInt64(value.size(), name), false);
  return writeBlob(value, name);
}

bool Xi::Serialization::Binary::OutputSerializer::writeBlob(ConstByteSpan value,
                                                            const std::string_view name) {
  XI_UNUSED(name);
  return Stream::writeStrict(m_stream, value);
}

bool Xi::Serialization::Binary::OutputSerializer::beginWriteComplex(const std::string_view name) {
  XI_UNUSED(name);
  return true;
}

bool Xi::Serialization::Binary::OutputSerializer::endWriteComplex() {
  return true;
}

bool Xi::Serialization::Binary::OutputSerializer::beginWriteVector(size_t size,
                                                                   const std::string_view name) {
  return writeUInt64(size, name);
}

bool Xi::Serialization::Binary::OutputSerializer::endWriteVector() {
  return true;
}

bool Xi::Serialization::Binary::OutputSerializer::beginWriteArray(size_t size,
                                                                  const std::string_view name) {
  XI_UNUSED(size, name);
  return true;
}

bool Xi::Serialization::Binary::OutputSerializer::endWriteArray() {
  return true;
}

bool Xi::Serialization::Binary::OutputSerializer::writeNull(const std::string_view name) {
  return writeBoolean(true, name);
}

bool Xi::Serialization::Binary::OutputSerializer::writeNotNull(const std::string_view name) {
  return writeBoolean(false, name);
}
