/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Serialization/Binary/InputSerializer.hpp"

#include <limits>

#include <Xi/Global.hh>
#include <Xi/Encoding/VarInt.hh>

#include <boost/endian/conversion.hpp>

namespace {
template <typename _IntT>
[[nodiscard]] bool readVarInt(_IntT& value, Xi::Stream::IInputStream& stream) {
  Xi::ByteVector bytes{};
  bytes.reserve(Xi::Encoding::VarInt::maximumEncodingSize<_IntT>());
  do {
    XI_RETURN_EC_IF(stream.isEndOfStream(), false);
    bytes.push_back(stream.take());
    XI_RETURN_EC_IF(bytes.size() > Xi::Encoding::VarInt::maximumEncodingSize<_IntT>(), false);
  } while (Xi::Encoding::VarInt::hasSuccessor(bytes.back()));

  _IntT reval = 0;
  XI_RETURN_EC_IF(Xi::Encoding::VarInt::decode(bytes, reval).isError(), false);
  value = reval;
  return true;
}

template <typename _IntT>
[[nodiscard]] bool readInt(_IntT& value, Xi::Stream::IInputStream& stream,
                           const Xi::Serialization::Binary::Options& options) {
  XI_RETURN_EC_IF_NOT(Xi::Stream::readStrict(stream, Xi::asByteSpan(&value, sizeof(_IntT))), false);
  value = Xi::Endianess::convert(value, options.endianess);
  return true;
}
}  // namespace

Xi::Serialization::Binary::InputSerializer::InputSerializer(Xi::Stream::IInputStream& stream,
                                                            Options options)
    : m_stream{stream}, m_options{options} {
}

bool Xi::Serialization::Binary::InputSerializer::readInt8(std::int8_t& value,
                                                          const std::string_view name) {
  XI_UNUSED(name);
  if (m_stream.isEndOfStream()) {
    return false;
  } else {
    const Byte _ = m_stream.take();
    value = *reinterpret_cast<const int8_t*>(&_);
    return true;
  }
}

bool Xi::Serialization::Binary::InputSerializer::readUInt8(std::uint8_t& value,
                                                           const std::string_view name) {
  XI_UNUSED(name);
  if (m_stream.isEndOfStream()) {
    return false;
  } else {
    const Byte _ = m_stream.take();
    value = *reinterpret_cast<const uint8_t*>(&_);
    return true;
  }
}

bool Xi::Serialization::Binary::InputSerializer::readInt16(std::int16_t& value,
                                                           const std::string_view name) {
  XI_UNUSED(name);
  return hasFlag(m_options.varInt, VarIntUsage::Int16) ? readVarInt(value, m_stream)
                                                       : readInt(value, m_stream, m_options);
}

bool Xi::Serialization::Binary::InputSerializer::readUInt16(std::uint16_t& value,
                                                            const std::string_view name) {
  XI_UNUSED(name);
  return hasFlag(m_options.varInt, VarIntUsage::UInt16) ? readVarInt(value, m_stream)
                                                        : readInt(value, m_stream, m_options);
}

bool Xi::Serialization::Binary::InputSerializer::readInt32(std::int32_t& value,
                                                           const std::string_view name) {
  XI_UNUSED(name);
  return hasFlag(m_options.varInt, VarIntUsage::Int32) ? readVarInt(value, m_stream)
                                                       : readInt(value, m_stream, m_options);
}

bool Xi::Serialization::Binary::InputSerializer::readUInt32(std::uint32_t& value,
                                                            const std::string_view name) {
  XI_UNUSED(name);
  return hasFlag(m_options.varInt, VarIntUsage::UInt32) ? readVarInt(value, m_stream)
                                                        : readInt(value, m_stream, m_options);
}

bool Xi::Serialization::Binary::InputSerializer::readInt64(std::int64_t& value,
                                                           const std::string_view name) {
  XI_UNUSED(name);
  return hasFlag(m_options.varInt, VarIntUsage::Int64) ? readVarInt(value, m_stream)
                                                       : readInt(value, m_stream, m_options);
}

bool Xi::Serialization::Binary::InputSerializer::readUInt64(std::uint64_t& value,
                                                            const std::string_view name) {
  XI_UNUSED(name);
  return hasFlag(m_options.varInt, VarIntUsage::UInt64) ? readVarInt(value, m_stream)
                                                        : readInt(value, m_stream, m_options);
}

bool Xi::Serialization::Binary::InputSerializer::readBoolean(bool& value,
                                                             const std::string_view name) {
  XI_UNUSED(name);
  std::uint8_t boolValue = 0;
  XI_RETURN_EC_IF_NOT(this->readUInt8(boolValue, name), false);
  if (boolValue == 0b01010101) {
    value = true;
    return true;
  } else if (boolValue == 0b10101010) {
    value = false;
    return true;
  } else {
    return false;
  }
}

bool Xi::Serialization::Binary::InputSerializer::readFloat(float& value,
                                                           const std::string_view name) {
  static_assert(std::numeric_limits<float>::is_iec559,
                "binary floating serialization is not supported on this architecture");
  union {
    float asFloating;
    ByteArray<sizeof(float)> asBlob;
  } data;
  XI_RETURN_EC_IF_NOT(this->readBlob(data.asBlob, name), false);
  value = data.asFloating;
  return true;
}

bool Xi::Serialization::Binary::InputSerializer::readDouble(double& value,
                                                            const std::string_view name) {
  static_assert(std::numeric_limits<double>::is_iec559,
                "binary floating serialization is not supported on this architecture");
  union {
    double asFloating;
    ByteArray<sizeof(double)> asBlob;
  } data;
  XI_RETURN_EC_IF_NOT(this->readBlob(data.asBlob, name), false);
  value = data.asFloating;
  return true;
}

bool Xi::Serialization::Binary::InputSerializer::readTypeTag(TypeTag& value,
                                                             const std::string_view name) {
  TypeTag::binary_type native = TypeTag::Null.binary();
  XI_RETURN_EC_IF_NOT(readUInt64(native, name), false);
  XI_RETURN_EC_IF(native == TypeTag::Null.binary(), false);
  value = TypeTag{native, TypeTag::NoTextTag};
  return true;
}

bool Xi::Serialization::Binary::InputSerializer::readFlag(Xi::Serialization::TypeTagVector& value,
                                                          const std::string_view name) {
  uint16_t nativeFlag = 0;
  XI_RETURN_EC_IF_NOT(readUInt16(nativeFlag, name), false);
  XI_RETURN_EC_IF(nativeFlag > (1 << TypeTag::maximumFlags()), false);
  for (size_t i = 0; (1 << i) <= nativeFlag; ++i) {
    if ((nativeFlag & (1 << i))) {
      value.emplace_back(i + 1, TypeTag::NoTextTag);
    }
  }
  return true;
}

bool Xi::Serialization::Binary::InputSerializer::readString(std::string& value,
                                                            const std::string_view name) {
  uint64_t size = 0;
  XI_RETURN_EC_IF_NOT(readUInt64(size, name), false);
  value.resize(size);
  XI_RETURN_EC_IF_NOT(readBlob(asByteSpan(value), name), false);
  return true;
}

bool Xi::Serialization::Binary::InputSerializer::readBinary(ByteVector& out,
                                                            const std::string_view name) {
  uint64_t size = 0;
  XI_RETURN_EC_IF_NOT(readUInt64(size, name), false);
  out.resize(size);
  XI_RETURN_EC_IF_NOT(readBlob(out, name), false);
  return true;
}

bool Xi::Serialization::Binary::InputSerializer::readBlob(ByteSpan out,
                                                          const std::string_view name) {
  XI_UNUSED(name);
  XI_RETURN_EC_IF_NOT(Stream::readStrict(m_stream, out), false);
  return true;
}

bool Xi::Serialization::Binary::InputSerializer::beginReadComplex(const std::string_view name) {
  XI_UNUSED(name);
  return true;
}

bool Xi::Serialization::Binary::InputSerializer::endReadComplex() {
  return true;
}

bool Xi::Serialization::Binary::InputSerializer::beginReadVector(size_t& size,
                                                                 const std::string_view name) {
  XI_RETURN_EC_IF_NOT(readUInt64(size, name), false);
  return true;
}

bool Xi::Serialization::Binary::InputSerializer::endReadVector() {
  return true;
}

bool Xi::Serialization::Binary::InputSerializer::beginReadArray(size_t size,
                                                                const std::string_view name) {
  XI_UNUSED(size, name);
  return true;
}

bool Xi::Serialization::Binary::InputSerializer::endReadArray() {
  return true;
}

bool Xi::Serialization::Binary::InputSerializer::checkNull(bool& value,
                                                           const std::string_view name) {
  return readBoolean(value, name);
}
