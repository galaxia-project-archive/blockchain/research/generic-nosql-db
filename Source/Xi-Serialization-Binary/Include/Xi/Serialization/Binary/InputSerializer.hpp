/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Stream/IInputStream.hpp>
#include <Xi/Serialization/IInputSerializer.hpp>

#include "Xi/Serialization/Binary/Options.hpp"

namespace Xi {
namespace Serialization {
namespace Binary {
class InputSerializer final : public IInputSerializer {
 public:
  explicit InputSerializer(Stream::IInputStream& stream, Options options = Options{});
  ~InputSerializer() override = default;

  [[nodiscard]] bool readInt8(std::int8_t& value, const std::string_view name) override;
  [[nodiscard]] bool readUInt8(std::uint8_t& value, const std::string_view name) override;
  [[nodiscard]] bool readInt16(std::int16_t& value, const std::string_view name) override;
  [[nodiscard]] bool readUInt16(std::uint16_t& value, const std::string_view name) override;
  [[nodiscard]] bool readInt32(std::int32_t& value, const std::string_view name) override;
  [[nodiscard]] bool readUInt32(std::uint32_t& value, const std::string_view name) override;
  [[nodiscard]] bool readInt64(std::int64_t& value, const std::string_view name) override;
  [[nodiscard]] bool readUInt64(std::uint64_t& value, const std::string_view name) override;

  [[nodiscard]] bool readBoolean(bool& value, const std::string_view name) override;

  [[nodiscard]] bool readFloat(float& value, const std::string_view name) override;
  [[nodiscard]] bool readDouble(double& value, const std::string_view name) override;

  [[nodiscard]] bool readTypeTag(TypeTag& value, const std::string_view name) override;
  [[nodiscard]] bool readFlag(TypeTagVector& value, const std::string_view name) override;

  [[nodiscard]] bool readString(std::string& value, const std::string_view name) override;
  [[nodiscard]] bool readBinary(ByteVector& out, const std::string_view name) override;

  [[nodiscard]] bool readBlob(ByteSpan out, const std::string_view name) override;

  [[nodiscard]] bool beginReadComplex(const std::string_view name) override;
  [[nodiscard]] bool endReadComplex() override;

  [[nodiscard]] bool beginReadVector(size_t& size, const std::string_view name) override;
  [[nodiscard]] bool endReadVector() override;

  [[nodiscard]] bool beginReadArray(size_t size, const std::string_view name) override;
  [[nodiscard]] bool endReadArray() override;

  [[nodiscard]] bool checkNull(bool& value, const std::string_view name) override;

 private:
  Stream::IInputStream& m_stream;
  Options m_options;
};
}  // namespace Binary
}  // namespace Serialization
}  // namespace Xi
