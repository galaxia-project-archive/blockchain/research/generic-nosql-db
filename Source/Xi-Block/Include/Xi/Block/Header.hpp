/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>

#include <Xi/Result.h>
#include <Xi/Time/Time.hpp>

#include "Xi/Block/Version.hpp"
#include "Xi/Block/Nonce.hpp"

namespace Xi {
namespace Block {

/*!
 *
 * \brief The Header struct wraps the minimum amount of data for the galaxia protocol.
 *
 * In galaxia the block header is only used to enable a proof of work blockchain structure. Any
 * other feature embedded by a specific chain MUST be contained by a block body. Further, to
 * validate the integrity of the block body, extensions are allowed to provide additional hashes of
 * their body entities to be emplaced into the header. The galaxia kernel gathers all body entity
 * hashes and emplace their merkle hash into the header itself.
 *
 * After a prefix hash of the header is computed. A prefix hash of a block hashes the entities
 * merkle hash and all header entities except the shell.
 *
 */
struct Header {};

}  // namespace Block
}  // namespace Xi
