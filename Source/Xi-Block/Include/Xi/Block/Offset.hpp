/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <functional>

#include <Xi/Global.hh>

namespace Xi {
namespace Block {

class Offset final {
 public:
  using value_type = int64_t;

 public:
  static Offset fromNative(const value_type value);

 public:
  Offset();
  XI_DEFAULT_COPY(Offset);
  ~Offset() = default;

  int64_t native() const;

  bool operator==(const Offset rhs) const;
  bool operator!=(const Offset rhs) const;
  bool operator<(const Offset rhs) const;
  bool operator<=(const Offset rhs) const;
  bool operator>(const Offset rhs) const;
  bool operator>=(const Offset rhs) const;

  Offset operator-() const;

 private:
  explicit Offset(value_type value);

 private:
  value_type m_native;
};

Offset operator+(const Offset lhs, const Offset rhs);
Offset& operator+=(Offset& lhs, const Offset rhs);
Offset operator-(const Offset lhs, const Offset rhs);
Offset& operator-=(Offset& lhs, const Offset rhs);

}  // namespace Block
}  // namespace Xi

namespace std {
template <> struct hash<Xi::Block::Offset> {
  std::size_t operator()(const Xi::Block::Offset offset) const;
};
}  // namespace std
