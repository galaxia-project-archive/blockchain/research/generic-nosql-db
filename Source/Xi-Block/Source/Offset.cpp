/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Block/Offset.hpp"

Xi::Block::Offset::Offset(Xi::Block::Offset::value_type value) : m_native{value} {
}

Xi::Block::Offset Xi::Block::Offset::fromNative(const Xi::Block::Offset::value_type value) {
  return Offset{value};
}

Xi::Block::Offset::Offset() : m_native{0} {
}

int64_t Xi::Block::Offset::native() const {
  return m_native;
}

bool Xi::Block::Offset::operator==(const Xi::Block::Offset rhs) const {
  return m_native == rhs.m_native;
}
bool Xi::Block::Offset::operator!=(const Xi::Block::Offset rhs) const {
  return m_native != rhs.m_native;
}
bool Xi::Block::Offset::operator<(const Xi::Block::Offset rhs) const {
  return m_native < rhs.m_native;
}
bool Xi::Block::Offset::operator<=(const Xi::Block::Offset rhs) const {
  return m_native <= rhs.m_native;
}
bool Xi::Block::Offset::operator>(const Xi::Block::Offset rhs) const {
  return m_native > rhs.m_native;
}
bool Xi::Block::Offset::operator>=(const Xi::Block::Offset rhs) const {
  return m_native >= rhs.m_native;
}

Xi::Block::Offset Xi::Block::Offset::operator-() const {
  return Offset::fromNative(-m_native);
}

size_t std::hash<Xi::Block::Offset>::operator()(const Xi::Block::Offset offset) const {
  return std::hash<Xi::Block::Offset::value_type>{}(offset.native());
}

Xi::Block::Offset Xi::Block::operator+(const Xi::Block::Offset lhs, const Xi::Block::Offset rhs) {
  return Offset::fromNative(lhs.native() + rhs.native());
}

Xi::Block::Offset& Xi::Block::operator+=(Xi::Block::Offset& lhs, const Xi::Block::Offset rhs) {
  return lhs = Offset::fromNative(lhs.native() + rhs.native());
}

Xi::Block::Offset Xi::Block::operator-(const Xi::Block::Offset lhs, const Xi::Block::Offset rhs) {
  return Offset::fromNative(lhs.native() - rhs.native());
}

Xi::Block::Offset& Xi::Block::operator-=(Xi::Block::Offset& lhs, const Xi::Block::Offset rhs) {
  return lhs = Offset::fromNative(lhs.native() - rhs.native());
}
