/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>
#include <string_view>

#include "Xi/Stream/InMemoryInputStream.hpp"
#include "Xi/Stream/ByteVectorOutputStream.hpp"
#include "Xi/Stream/ByteArrayOutputStream.hpp"
#include "Xi/Stream/StringOutputStream.hpp"

namespace Xi {
namespace Stream {
UniqueInMemoryInputStream asInputStream(const std::string_view source);
UniqueInMemoryInputStream asInputStream(ConstByteSpan source);

UniqueStringOutputStream asOutputStream(std::string &dest);
UniqueByteVectorOutputStream asOutputStream(ByteVector &dest);
UniqueByteArrayOutputStream asOutputStream(ByteSpan dest);
}  // namespace Stream
}  // namespace Xi
