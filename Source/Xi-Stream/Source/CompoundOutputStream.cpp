/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Stream/CompoundOutputStream.hpp"

#include <utility>

Xi::Stream::CompoundOutputStream::CompoundOutputStream(
    std::vector<Xi::Stream::UniqueIOutputStream> dests) {
  for (auto& dest : dests) {
    if (dest) {
      m_dests.emplace_back(std::move(dest));
    }
  }
}

size_t Xi::Stream::CompoundOutputStream::write(Xi::ConstByteSpan buffer) {
  XI_RETURN_EC_IF(buffer.empty(), 0);
  XI_RETURN_EC_IF(m_dests.empty(), 0);
  size_t nWritten = 0;
  do {
    const size_t iWritten = m_dests.front()->write(buffer);
    if (iWritten == 0) {
      m_dests.pop_front();
      if (nWritten > 0) {
        break;
      } else {
        continue;
      }
    }
    buffer = buffer.slice(nWritten);
    if (buffer.empty()) {
      break;
    } else if (nWritten > 0) {
      break;
    }
  } while (!m_dests.empty());
  return nWritten;
}

bool Xi::Stream::CompoundOutputStream::flush() {
  XI_RETURN_EC_IF(m_dests.empty(), false);
  return m_dests.front()->flush();
}
