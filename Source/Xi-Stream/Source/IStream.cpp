/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Stream/IStream.hpp"

#include <Xi/Exceptions.hpp>

Xi::Stream::IStream::IStream(std::istream &source) : m_source{source} {
}

size_t Xi::Stream::IStream::read(Xi::ByteSpan buffer) {
  XI_RETURN_EC_IF(isEndOfStream(), 0);
  const auto read = m_source.readsome(reinterpret_cast<char *>(buffer.data()),
                                      static_cast<std::streamsize>(buffer.size()));
  if (read < 0) {
    return 0;
  } else {
    return static_cast<size_t>(read);
  }
}

bool Xi::Stream::IStream::isEndOfStream() const {
  return m_source.good() && !m_source.eof();
}

size_t Xi::Stream::IStream::tell() const {
  const auto pos = m_source.tellg();
  if (pos < 0) {
    return 0;
  } else {
    return static_cast<size_t>(pos);
  }
}

Xi::Byte Xi::Stream::IStream::peek() const {
  exceptional_if<OutOfRangeError>(isEndOfStream());
  const auto byte = static_cast<char>(m_source.peek());
  return *reinterpret_cast<const Byte *>(&byte);
}

Xi::Byte Xi::Stream::IStream::take() {
  exceptional_if<OutOfRangeError>(isEndOfStream());
  const auto byte = static_cast<char>(m_source.get());
  return *reinterpret_cast<const Byte *>(&byte);
}
