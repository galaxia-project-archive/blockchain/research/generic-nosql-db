/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Stream/OStream.hpp"

Xi::Stream::OStream::OStream(std::ostream &dest) : m_dest{dest} {
}

size_t Xi::Stream::OStream::write(Xi::ConstByteSpan buffer) {
  try {
    m_dest.write(reinterpret_cast<const char *>(buffer.data()),
                 static_cast<std::streamsize>(buffer.size()));
    return buffer.size();
  } catch (...) {
    return 0;
  }
}

bool Xi::Stream::OStream::flush() {
  try {
    m_dest.flush();
    return true;
  } catch (...) {
    return false;
  }
}
