/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>

#include "Xi/Blockchain/Version.hpp"

namespace Xi {
namespace Blockchain {
namespace Block {

/*!
 *
 * \brief The IExtension class wraps extendable blockchain facilities.
 *
 * Extensions are loaded from a plugin.
 *
 */
class IExtension {
 public:
  virtual ~IExtension() = 0;

  /*!
   *
   * \brief name is an unique identifier in regards to other extensions
   * \return the extension identifier unique to all other extensions, but not different versions of
   * the same extension.
   *
   */
  [[nodiscard]] virtual std::string name() = 0;

  /*!
   *
   * \brief version the current version of the extension.
   * \return version of the extension (not null)
   *
   */
  [[nodiscard]] virtual Version version() = 0;
};

}  // namespace Block
}  // namespace Blockchain
}  // namespace Xi
