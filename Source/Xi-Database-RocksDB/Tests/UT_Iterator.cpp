/* ==============================================================================================
 * *
 *                                                                                                *
 *                                       Xi Blockchain *
 *                                                                                                *
 * ----------------------------------------------------------------------------------------------
 * * This file is part of the Galaxia Project - Xi Blockchain *
 * ----------------------------------------------------------------------------------------------
 * *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the * GNU General Public License as published by the Free
 * Software Foundation, either version 3 of   * the License, or (at your option)
 * any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY;      * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.      * See the GNU General Public License
 * for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with
 * this program.     * If not, see <https://www.gnu.org/licenses/>. *
 *                                                                                                *
 * ==============================================================================================
 */

#include "DatabaseFixture.hpp"

#include <map>
#include <cstring>
#include <iterator>

#include <iostream>

TEST_F(Xi_Database_RocksDB, Iterator_IsSorted) {
  auto table = blobIndices();

  std::default_random_engine eng;
  std::uniform_int_distribution<IndexedBlobs::key_type> dist{};

  std::map<IndexedBlobs::key_type, IndexedBlobs::value_type> values;

  for (size_t i = 0; i < 50; ++i) {
    Xi_Database_RocksDB_Blob value;
    auto key = dist(eng);
    random(value.span());
    if (values.find(key) != end(values)) {
      EXPECT_EQ(table->put(key, values[key]), QueryError::AlreadyExists);

    } else {
      EXPECT_EQ(table->put(key, values[key]), QueryError::Success);
    }
  }

  auto valueCopy = values;

  auto iterator = table->begin();
  ASSERT_FALSE(iterator.get() == nullptr);
  while (!values.empty() && !iterator->isEnd()) {
    auto i = begin(values);
    ASSERT_EQ(iterator->error(), QueryError::Success);

    auto iKey = iterator->key().takeOrThrow();
    auto iValue = iterator->value().takeOrThrow();
    EXPECT_EQ(i->first, iKey);
    EXPECT_EQ(std::memcmp(i->second.data(), iValue.data(), iValue.size()), 0);
    values.erase(values.find(iKey));
    iterator->advance();
  }
  EXPECT_TRUE(values.empty());
  EXPECT_TRUE(iterator->isEnd());
}

TEST_F(Xi_Database_RocksDB, Iterator_IsEndOnEmpty) {
  auto table = blobIndices();
  auto iterator = table->begin();
  EXPECT_TRUE(iterator->isEnd());
}

TEST_F(Xi_Database_RocksDB, Iterator_IsOffseted) {
  auto table = strings_0();
  auto values = insertTestStrings();
  auto begin = std::next(values.begin(), 2);
  auto iterator = table->begin(begin->first);
  while (begin != values.end() && !iterator->isEnd()) {
    EXPECT_TRUE(begin->first == iterator->key().takeOrThrow());

    std::advance(begin, 1);
    iterator->advance();
  }
  EXPECT_TRUE(begin == end(values));
}

TEST_F(Xi_Database_RocksDB, Iterator_CanSeek) {
  auto table = strings_0();
  auto values = insertTestStrings();
  auto begin = std::next(values.begin(), 2);
  while (begin != values.end()) {
    auto iterator = table->begin();
    ASSERT_FALSE(iterator.get() == nullptr);
    EXPECT_EQ(iterator->seek(begin->first), QueryError::Success);
    EXPECT_FALSE(iterator->isEnd());
    EXPECT_TRUE(begin->first == iterator->key().takeOrThrow());

    std::advance(begin, 1);
    iterator->advance();
  }
}

TEST_F(Xi_Database_RocksDB, Iterator_Range) {
  auto table = blobIndices();
  auto values = insertRandomIndexedBlobs(10);
  auto begin = table->begin();
  ASSERT_FALSE(begin.get() == nullptr);
  auto end = table->end();
  ASSERT_FALSE(end.get() == nullptr);

  size_t count = 0;
  while (*begin != *end) {
    count++;
    begin->advance();
  }
  EXPECT_EQ(*begin, *end);
  EXPECT_EQ(count, 10);
}

TEST_F(Xi_Database_RocksDB, Iterator_SubRange) {
  auto table = blobIndices();
  auto values = insertRandomIndexedBlobs(30);
  auto begin = table->begin(std::next(values.begin(), 10)->first);
  ASSERT_FALSE(begin.get() == nullptr);
  auto end = table->end(std::next(values.begin(), 20)->first);
  ASSERT_FALSE(end.get() == nullptr);

  size_t count = 0;
  while (*begin != *end) {
    count++;
    begin->advance();
  }
  EXPECT_EQ(*begin, *end);
  EXPECT_EQ(count, 10);
}
