/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "DatabaseFixture.hpp"

#include <algorithm>
#include <cstring>
#include <string>
#include <vector>

#include <Xi/FileSystem.h>
#include <Xi/Log/Log.hpp>

#include <Xi/Crypto/Random/Random.hh>

void Xi_Database_RocksDB::SetUp() {
  Xi::Log::Registry::put(nullptr);

  ASSERT_FALSE(Xi::FileSystem::removeDircetoryIfExists("./data-dir"));
  database = RocksDB::Database::create();
  ASSERT_TRUE(database->initialize());
}

void Xi_Database_RocksDB::TearDown() {
  ASSERT_TRUE(database->shutdown());
  ASSERT_FALSE(Xi::FileSystem::removeDircetoryIfExists("./data-dir"));
}

std::map<Xi_Database_RocksDB::IndexedBlobs::key_type, Xi_Database_RocksDB::IndexedBlobs::value_type>
Xi_Database_RocksDB::insertRandomIndexedBlobs(size_t count) {
  std::default_random_engine eng;
  std::uniform_int_distribution<IndexedBlobs::key_type> dist{};

  auto table = blobIndices();
  std::map<IndexedBlobs::key_type, IndexedBlobs::value_type> values;

  for (size_t i = 0; i < count; ++i) {
    auto key = dist(eng);
    random(values[key].span());
    (void)table->put(key, values[key]);
  }
  return values;
}

std::map<std::string, std::string> Xi_Database_RocksDB::insertTestStrings() {
  auto table_0 = strings_0();
  std::vector<std::string> values{"", u8"猫猫", "  ", " ", "- ", " -"};

  std::map<std::string, std::string> reval;
  for (const auto& iValue : values) {
    for (const auto& jValue : values) {
      (void)table_0->put(iValue, jValue);
      reval[iValue] = jValue;
    }
  }
  return reval;
}

SharedTable<Xi_Database_RocksDB::IndexedBlobs> Xi_Database_RocksDB::blobIndices() {
  return database->getTable<IndexedBlobs>();
}

SharedTable<Xi_Database_RocksDB::Strings_1> Xi_Database_RocksDB::strings_1() {
  return database->getTable<Strings_1>();
}
SharedTable<Xi_Database_RocksDB::Strings_0> Xi_Database_RocksDB::strings_0() {
  return database->getTable<Strings_0>();
}

void Xi_Database_RocksDB::random(ByteSpan out) const {
  ASSERT_EQ(Xi::Crypto::Random::generate(out), Xi::Crypto::Random::RandomError::Success);
}

bool Xi_Database_RocksDB::compare(const Xi_Database_RocksDB_Blob& lhs,
                                  const Xi_Database_RocksDB_Blob& rhs) const {
  return std::memcmp(lhs.data(), rhs.data(), Xi_Database_RocksDB_Blob::bytes()) == 0;
}
