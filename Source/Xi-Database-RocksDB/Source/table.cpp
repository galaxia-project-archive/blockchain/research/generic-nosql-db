/* ==============================================================================================
 * *
 *                                                                                                *
 *                                       Xi Blockchain *
 *                                                                                                *
 * ----------------------------------------------------------------------------------------------
 * * This file is part of the Galaxia Project - Xi Blockchain *
 * ----------------------------------------------------------------------------------------------
 * *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the * GNU General Public License as published by the Free
 * Software Foundation, either version 3 of   * the License, or (at your option)
 * any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY;      * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.      * See the GNU General Public License
 * for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with
 * this program.     * If not, see <https://www.gnu.org/licenses/>. *
 *                                                                                                *
 * ==============================================================================================
 */

#include "Table.hpp"

#include <Xi/Exceptions.hpp>
#include <Xi/Serialization/Binary/InputSerializer.hpp>
#include <Xi/Serialization/Binary/OutputSerializer.hpp>

#define XI_LOG_VERBOSE
#include <Xi/Log/Log.hpp>

XI_LOGGER("Database/RocksDB/Table")

#include "Iterator.hpp"

Xi::Database::RocksDB::Table::Table(rocksdb::DB *db, const TablePrefix &prefix)
    : ITable(prefix), m_db{db}, m_writeOptions{} {
  exceptional_if<NullArgumentError>(db == nullptr);
  m_writeOptions.sync = true;
  m_writeOptions.no_slowdown = false;
  m_writeOptions.disableWAL = false;
}

Xi::Database::RocksDB::Table::~Table() {
  release();
}

void Xi::Database::RocksDB::Table::release() {
}

Xi::Serialization::UniqueIInputSerializer Xi::Database::RocksDB::Table::createKeyInputSerailizer(
    Xi::Stream::IInputStream &input) const {
  return std::make_unique<Serialization::Binary::InputSerializer>(
      input, Serialization::Binary::Options{Serialization::Binary::VarIntUsage::None,
                                            Endianess::Type::Big});
}

Xi::Serialization::UniqueIOutputSerializer Xi::Database::RocksDB::Table::createKeyOutputSerailizer(
    Xi::Stream::IOutputStream &output) const {
  return std::make_unique<Serialization::Binary::OutputSerializer>(
      output, Serialization::Binary::Options{Serialization::Binary::VarIntUsage::None,
                                             Endianess::Type::Big});
}

Xi::Serialization::UniqueIInputSerializer Xi::Database::RocksDB::Table::createValueInputSerailizer(
    Stream::IInputStream &input) const {
  return std::make_unique<Serialization::Binary::InputSerializer>(
      input, Serialization::Binary::Options{Serialization::Binary::VarIntUsage::All});
}

Xi::Serialization::UniqueIOutputSerializer
Xi::Database::RocksDB::Table::createValueOutputSerailizer(Stream::IOutputStream &output) const {
  return std::make_unique<Serialization::Binary::OutputSerializer>(
      output, Serialization::Binary::Options{Serialization::Binary::VarIntUsage::All});
}

Xi::Database::QueryError Xi::Database::RocksDB::Table::get(Xi::ConstByteSpan key,
                                                           Xi::Stream::IOutputStream &valueOutput) {
  std::string value;
  rocksdb::ReadOptions options{};

  auto status = m_db->Get(
      options, rocksdb::Slice{reinterpret_cast<const char *>(key.data()), key.size()}, &value);
  XI_VERBOSE_IF(!status.ok(), "GET failed: {}", status.ToString());
  XI_RETURN_EC_IF_NOT(status.ok(), toQueryError(status));
  XI_RETURN_EC_IF_NOT(Stream::writeStrict(valueOutput, asConstByteSpan(value)),
                      QueryError::SerializationError);
  return QueryError::Success;
}

Xi::Database::QueryError Xi::Database::RocksDB::Table::put(Xi::ConstByteSpan key,
                                                           Xi::ConstByteSpan value) {
  rocksdb::Slice keySlice{reinterpret_cast<const char *>(key.data()), key.size_bytes()};
  rocksdb::Slice valueSlice{reinterpret_cast<const char *>(value.data()), value.size_bytes()};
  auto status = m_db->Put(m_writeOptions, keySlice, valueSlice);
  XI_VERBOSE_IF(!status.ok(), "PUT failed: {}", status.ToString());
  XI_RETURN_EC_IF_NOT(status.ok(), toQueryError(status));
  return QueryError::Success;
}

Xi::Database::QueryError Xi::Database::RocksDB::Table::del(Xi::ConstByteSpan key) {
  rocksdb::Slice keySlice{reinterpret_cast<const char *>(key.data()), key.size_bytes()};
  auto status = m_db->SingleDelete(m_writeOptions, keySlice);
  XI_VERBOSE_IF(!status.ok(), "ERASE failed: {}", status.ToString());
  XI_RETURN_EC_IF_NOT(status.ok(), toQueryError(status));
  return QueryError::Success;
}

Xi::Database::QueryError Xi::Database::RocksDB::Table::del(Xi::ConstByteSpan begin,
                                                           Xi::ConstByteSpan exclusiveEnd) {
  rocksdb::Slice beginSlice{reinterpret_cast<const char *>(begin.data()), begin.size_bytes()};
  rocksdb::Slice endSlice{reinterpret_cast<const char *>(exclusiveEnd.data()),
                          exclusiveEnd.size_bytes()};
  auto status = m_db->DeleteRange(m_writeOptions, nullptr, beginSlice, endSlice);
  XI_VERBOSE_IF(!status.ok(), "ERASE_RANGE failed: {}", status.ToString());
  XI_RETURN_EC_IF_NOT(status.ok(), toQueryError(status));
  return QueryError::Success;
}

Xi::Database::UniqueIIterator Xi::Database::RocksDB::Table::range(Xi::ConstByteSpan start) {
  rocksdb::ReadOptions options{};
  rocksdb::Iterator *iter = m_db->NewIterator(options);
  if (iter == nullptr) {
    return nullptr;
  } else {
    rocksdb::Slice beginSlice{reinterpret_cast<const char *>(start.data()), start.size_bytes()};
    iter->Seek(beginSlice);
    return std::make_unique<Iterator>(iter);
  }
}

Xi::Database::QueryError Xi::Database::RocksDB::toQueryError(const rocksdb::Status &status) {
  XI_RETURN_EC_IF(status.ok(), QueryError::Success);
  XI_RETURN_EC_IF(status.IsNotFound(), QueryError::NotFound);
  XI_RETURN_EC_IF(status.IsBusy(), QueryError::Busy);
  XI_RETURN_EC_IF(status.IsNoSpace(), QueryError::OutOfMemory);
  XI_WARN("Unexpected database error: {}", status.ToString());
  return QueryError::InternalError;
}
