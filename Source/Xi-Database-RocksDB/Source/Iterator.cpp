/* ==============================================================================================
 * *
 *                                                                                                *
 *                                       Xi Blockchain *
 *                                                                                                *
 * ----------------------------------------------------------------------------------------------
 * * This file is part of the Galaxia Project - Xi Blockchain *
 * ----------------------------------------------------------------------------------------------
 * *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the * GNU General Public License as published by the Free
 * Software Foundation, either version 3 of   * the License, or (at your option)
 * any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY;      * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.      * See the GNU General Public License
 * for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with
 * this program.     * If not, see <https://www.gnu.org/licenses/>. *
 *                                                                                                *
 * ==============================================================================================
 */

#include "Iterator.hpp"

#include <cassert>

#include <Xi/Exceptions.hpp>
#include <Xi/Database/TablePrefix.hpp>

#include "Table.hpp"

Xi::Database::RocksDB::Iterator::Iterator(
    Xi::Database::RocksDB::Iterator::native_iterator *iterator) {
  exceptional_if<NullArgumentError>(iterator == nullptr);
  m_iterator.reset(iterator);
}

bool Xi::Database::RocksDB::Iterator::isEnd() const {
  return !m_iterator->Valid();
}

Xi::Database::QueryError Xi::Database::RocksDB::Iterator::error() const {
  return toQueryError(m_iterator->status());
}

void Xi::Database::RocksDB::Iterator::advance() {
  return m_iterator->Next();
}

void Xi::Database::RocksDB::Iterator::seek(Xi::ConstByteSpan offset) {
  rocksdb::Slice slice{reinterpret_cast<const char *>(offset.data()), offset.size_bytes()};
  m_iterator->Seek(slice);
}

Xi::Database::TablePrefix Xi::Database::RocksDB::Iterator::prefix() const {
  const auto slice = m_iterator->key();
  assert(slice.size() > TablePrefix::bytes());
  return TablePrefix{*reinterpret_cast<const uint16_t *>(slice.data())};
}

Xi::ConstByteSpan Xi::Database::RocksDB::Iterator::key() const {
  const auto slice = m_iterator->key();
  assert(slice.size() >= TablePrefix::bytes());
  return ConstByteSpan{reinterpret_cast<const Byte *>(slice.data() + TablePrefix::bytes()),
                       slice.size() - TablePrefix::bytes()};
}

Xi::ConstByteSpan Xi::Database::RocksDB::Iterator::value() const {
  const auto slice = m_iterator->value();
  return ConstByteSpan{reinterpret_cast<const Byte *>(slice.data()), slice.size()};
}
