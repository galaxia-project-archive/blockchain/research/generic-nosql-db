/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <rocksdb/db.h>

#include <Xi/Database/IWriteBatch.hpp>

namespace Xi {
namespace Database {
namespace RocksDB {

class WriteBatch final : public IWriteBatch {
 public:
  explicit WriteBatch(std::unique_ptr<rocksdb::WriteBatch> handle);

  [[nodiscard]] bool put(ConstByteSpan key, ConstByteSpan value) override;
  [[nodiscard]] bool del(ConstByteSpan key) override;

 private:
  std::unique_ptr<rocksdb::WriteBatch> m_handle;
};

}  // namespace RocksDB
}  // namespace Database
}  // namespace Xi
