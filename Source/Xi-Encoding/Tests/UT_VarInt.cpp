/* ------------------------------------------------------------------------------------------------------------------
 * *
 *                                                                                                                    *
 *                                                   MIT Licenese *
 *                                                                                                                    *
 * ------------------------------------------------------------------------------------------------------------------
 * *
 *                                                                                                                    *
 * Copyright 2019 Michael Herwig <michael.herwig@hotmail.de> *
 *                                                                                                                    *
 * ------------------------------------------------------------------------------------------------------------------
 * *
 *                                                                                                                    *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated       * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation    * the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and   * to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:                 *
 *                                                                                                                    *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of   * the Software. *
 *                                                                                                                    *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO   * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE     * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF          * CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS *
 * IN THE SOFTWARE. *
 *                                                                                                                    *
 * ------------------------------------------------------------------------------------------------------------------
 */

#include <limits>
#include <random>
#include <array>
#include <algorithm>
#include <type_traits>

#include <gmock/gmock.h>

#include <Xi/Encoding/VarInt.hh>

#define XI_TEST_SUITE Xi_Encoding_VarInt

namespace {
template <typename _IntT> void generic_decode_encode_test() {
  using namespace Xi::Encoding::VarInt;

  const _IntT max = std::numeric_limits<_IntT>::max();
  const _IntT min = std::numeric_limits<_IntT>::min();
  const _IntT zero = 0;
  std::array<_IntT, 200> randoms{};
  {
    std::default_random_engine e{};
    std::uniform_int_distribution<_IntT> dist{};
    for (auto &i : randoms) {
      i = (_IntT)dist(e);
    }
  }

  Xi::ByteArray<32> buffer{};

  const auto testValue = [&buffer](_IntT value) {
    _IntT placeholder;
    buffer.fill(0);
    auto encodingSize = encode(value, buffer).valueOrThrow();
    ASSERT_GT(encodingSize, 0u);
    EXPECT_LE(encodingSize, maximumEncodingSize<_IntT>());
    EXPECT_EQ(buffer[encodingSize], 0u);
    EXPECT_FALSE(hasSuccessor(buffer[encodingSize - 1]));
    ASSERT_FALSE(decode(buffer, placeholder).isError());
    EXPECT_EQ(value, placeholder);
    if (encodingSize > 1) {
      for (size_t i = 0; i < encodingSize - 2; ++i) {
        EXPECT_TRUE(hasSuccessor(buffer[i]));
      }

      buffer[encodingSize - 1] = 0;
      EXPECT_TRUE(decode(buffer, placeholder).isError());
    }
  };

  testValue(min);
  testValue(max);
  testValue(min + 1);
  testValue(max - 1);
  testValue(zero);
  for (auto i : randoms) {
    testValue(i);
    testValue(~i);
  }
}

template <typename _IntT> void generic_out_of_memory_test() {
  using namespace Xi::Encoding::VarInt;

  Xi::ByteArray<1> buffer{0xFF};
  Xi::ByteVector emptyBuffer{};
  ASSERT_EQ(emptyBuffer.size(), 0);

  _IntT placeholder = 0;
  EXPECT_TRUE(decode(buffer, placeholder).isError());
  EXPECT_TRUE(decode(emptyBuffer, placeholder).isError());

  placeholder = std::numeric_limits<_IntT>::max();
  EXPECT_TRUE(encode(placeholder, emptyBuffer).isError());
}

template <typename _IntT> void generic_overflow_test() {
  using namespace Xi::Encoding::VarInt;

  Xi::ByteArray<32> buffer{};
  _IntT bigNum = std::numeric_limits<_IntT>::max();
  const auto encodingSize = encode(bigNum, buffer).takeOrThrow();
  ASSERT_GT(encodingSize, 0u);
  buffer[encodingSize - 1] = 0xFF;
  EXPECT_TRUE(decode(buffer, bigNum).isError());
}
}  // namespace

TEST(XI_TEST_SUITE, DecodeEncodeInt16) {
  generic_decode_encode_test<int16_t>();
}
TEST(XI_TEST_SUITE, DecodeEncodeUInt16) {
  generic_decode_encode_test<uint16_t>();
}
TEST(XI_TEST_SUITE, DecodeEncodeInt32) {
  generic_decode_encode_test<int32_t>();
}
TEST(XI_TEST_SUITE, DecodeEncodeUInt32) {
  generic_decode_encode_test<uint32_t>();
}
TEST(XI_TEST_SUITE, DecodeEncodeInt64) {
  generic_decode_encode_test<int64_t>();
}
TEST(XI_TEST_SUITE, DecodeEncodeUInt64) {
  generic_decode_encode_test<uint64_t>();
}

TEST(XI_TEST_SUITE, OutOfMemoryInt16) {
  generic_out_of_memory_test<int16_t>();
}
TEST(XI_TEST_SUITE, OutOfMemoryUInt16) {
  generic_out_of_memory_test<uint16_t>();
}
TEST(XI_TEST_SUITE, OutOfMemoryInt32) {
  generic_out_of_memory_test<int32_t>();
}
TEST(XI_TEST_SUITE, OutOfMemoryUInt32) {
  generic_out_of_memory_test<uint32_t>();
}
TEST(XI_TEST_SUITE, OutOfMemoryInt64) {
  generic_out_of_memory_test<int64_t>();
}
TEST(XI_TEST_SUITE, OutOfMemoryUInt64) {
  generic_out_of_memory_test<uint64_t>();
}

TEST(XI_TEST_SUITE, OverflowInt16) {
  generic_overflow_test<int16_t>();
}
TEST(XI_TEST_SUITE, OverflowUInt16) {
  generic_overflow_test<uint16_t>();
}
TEST(XI_TEST_SUITE, OverflowInt32) {
  generic_overflow_test<int32_t>();
}
TEST(XI_TEST_SUITE, OverflowUInt32) {
  generic_overflow_test<uint32_t>();
}
TEST(XI_TEST_SUITE, OverflowInt64) {
  generic_overflow_test<int64_t>();
}
TEST(XI_TEST_SUITE, OverflowUInt64) {
  generic_overflow_test<uint64_t>();
}
