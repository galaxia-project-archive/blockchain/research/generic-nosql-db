/* ------------------------------------------------------------------------------------------------------------------
 * *
 *                                                                                                                    *
 *                                                   MIT Licenese *
 *                                                                                                                    *
 * ------------------------------------------------------------------------------------------------------------------
 * *
 *                                                                                                                    *
 * Copyright 2019 Michael Herwig <michael.herwig@hotmail.de> *
 *                                                                                                                    *
 * ------------------------------------------------------------------------------------------------------------------
 * *
 *                                                                                                                    *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated       * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation    * the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and   * to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:                 *
 *                                                                                                                    *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of   * the Software. *
 *                                                                                                                    *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO   * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE     * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF          * CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS *
 * IN THE SOFTWARE. *
 *                                                                                                                    *
 * ------------------------------------------------------------------------------------------------------------------
 */

#include "Xi/Encoding/VarInt.hh"

#include <limits.h>

#include "Xi/Global.hh"

static const xi_byte_t xi_leb_next_mask = 0x80;
static const xi_byte_t xi_leb_next_shift = 7;
static const xi_byte_t xi_leb_negative_mask = 0x40;
static const xi_byte_t xi_leb_negative_shift = 6;
static const xi_byte_t xi_leb_first_byte_mask = 0xC0;

size_t xi_encoding_varint_decode_int16(const xi_byte_t *source, const size_t count, int16_t *out) {
  if (count < 1) {
    return XI_VARINT_DECODE_OUT_OF_MEMORY;
  }

  xi_byte_t byte = source[0];

  if (byte == xi_leb_negative_mask) {
    *out = SHRT_MIN;
    return 1;
  }

  *out = (int16_t)((byte & (~xi_leb_first_byte_mask)));
  size_t i = 1;

  if ((byte & xi_leb_next_mask) != 0) {
    for (size_t shift = xi_leb_negative_shift; 1; shift += xi_leb_next_shift) {
      if (i >= count) {
        return XI_VARINT_DECODE_OUT_OF_MEMORY;
      } else if (shift > 16) {
        return XI_VARINT_DECODE_OVERFLOW;
      }
      byte = source[i++];
      if (byte == 0) {
        return XI_VARINT_DECODE_NONE_CANONICAL;
      }
      *out |= ((int16_t)((byte & (~xi_leb_next_mask))) << shift);
      if ((byte & xi_leb_next_mask) == 0) {
        break;
      }
    }
  }

  if ((source[0] & xi_leb_negative_mask) != 0) {
    *out *= -1;
  }

  return i;
}

size_t xi_encoding_varint_decode_uint16(const xi_byte_t *source, const size_t count,
                                        uint16_t *out) {
  *out = 0;
  size_t i = 0;
  for (size_t shift = 0; 1; shift += xi_leb_next_shift) {
    if (i >= count) {
      return XI_VARINT_DECODE_OUT_OF_MEMORY;
    } else if (shift > 16) {
      return XI_VARINT_DECODE_OVERFLOW;
    }
    xi_byte_t byte = source[i++];
    if (byte == 0 && shift != 0) {
      return XI_VARINT_DECODE_NONE_CANONICAL;
    }
    *out |= ((uint16_t)((byte & (~xi_leb_next_mask))) << shift);
    if ((byte & xi_leb_next_mask) == 0) {
      break;
    }
  }
  return i;
}

size_t xi_encoding_varint_decode_int32(const xi_byte_t *source, const size_t count, int32_t *out) {
  if (count < 1) {
    return XI_VARINT_DECODE_OUT_OF_MEMORY;
  }

  xi_byte_t byte = source[0];

  if (byte == xi_leb_negative_mask) {
    *out = INT_MIN;
    return 1;
  }

  *out = (int32_t)((byte & (~xi_leb_first_byte_mask)));
  size_t i = 1;

  if ((byte & xi_leb_next_mask) != 0) {
    for (size_t shift = xi_leb_negative_shift; 1; shift += xi_leb_next_shift) {
      if (i >= count) {
        return XI_VARINT_DECODE_OUT_OF_MEMORY;
      } else if (shift > 32) {
        return XI_VARINT_DECODE_OVERFLOW;
      }
      byte = source[i++];
      if (byte == 0) {
        return XI_VARINT_DECODE_NONE_CANONICAL;
      }
      *out |= ((int32_t)((byte & (~xi_leb_next_mask))) << shift);
      if ((byte & xi_leb_next_mask) == 0) {
        break;
      }
    }
  }

  if ((source[0] & xi_leb_negative_mask) != 0) {
    *out *= -1;
  }

  return i;
}

size_t xi_encoding_varint_decode_uint32(const xi_byte_t *source, const size_t count,
                                        uint32_t *out) {
  *out = 0;
  size_t i = 0;
  for (size_t shift = 0; 1; shift += xi_leb_next_shift) {
    if (i >= count) {
      return XI_VARINT_DECODE_OUT_OF_MEMORY;
    } else if (shift > 32) {
      return XI_VARINT_DECODE_OVERFLOW;
    }
    xi_byte_t byte = source[i++];
    if (byte == 0 && shift != 0) {
      return XI_VARINT_DECODE_NONE_CANONICAL;
    }
    *out |= ((uint32_t)((byte & (~xi_leb_next_mask))) << shift);
    if ((byte & xi_leb_next_mask) == 0) {
      break;
    }
  }
  return i;
}

size_t xi_encoding_varint_decode_int64(const xi_byte_t *source, const size_t count, int64_t *out) {
  if (count < 1) {
    return XI_VARINT_DECODE_OUT_OF_MEMORY;
  }

  xi_byte_t byte = source[0];

  if (byte == xi_leb_negative_mask) {
    *out = LLONG_MIN;
    return 1;
  }
  *out = (int64_t)((byte & (~xi_leb_first_byte_mask)));
  size_t i = 1;

  if ((byte & xi_leb_next_mask) != 0) {
    for (size_t shift = xi_leb_negative_shift; 1; shift += xi_leb_next_shift) {
      if (i >= count) {
        return XI_VARINT_DECODE_OUT_OF_MEMORY;
      } else if (shift > 64) {
        return XI_VARINT_DECODE_OVERFLOW;
      }
      byte = source[i++];
      if (byte == 0) {
        return XI_VARINT_DECODE_NONE_CANONICAL;
      }
      *out |= ((int64_t)((byte & (~xi_leb_next_mask))) << shift);
      if ((byte & xi_leb_next_mask) == 0) {
        break;
      }
    }
  }

  if ((source[0] & xi_leb_negative_mask) != 0) {
    *out *= -1;
  }

  return i;
}

size_t xi_encoding_varint_decode_uint64(const xi_byte_t *source, const size_t count,
                                        uint64_t *out) {
  *out = 0;
  size_t i = 0;
  for (size_t shift = 0; 1; shift += xi_leb_next_shift) {
    if (i >= count) {
      return XI_VARINT_DECODE_OUT_OF_MEMORY;
    } else if (shift > 64) {
      return XI_VARINT_DECODE_OVERFLOW;
    }
    xi_byte_t byte = source[i++];

    if (byte == 0 && shift != 0) {
      return XI_VARINT_DECODE_NONE_CANONICAL;
    }
    *out |= ((uint64_t)((byte & (~xi_leb_next_mask))) << shift);
    if ((byte & xi_leb_next_mask) == 0) {
      break;
    }
  }
  return i;
}

size_t xi_encoding_varint_encode_int16(int16_t value, xi_byte_t *dest, const size_t count) {
  if (count < 1) {
    return XI_VARINT_ENCODE_OUT_OF_MEMORY;
  }

  if (value == SHRT_MIN) {
    dest[0] = xi_leb_negative_mask;
    return 1;
  }

  if (value < 0) {
    dest[0] = xi_leb_negative_mask;
    value *= -1;
  } else {
    dest[0] = 0;
  }

  size_t i = 0;
  if (value >= xi_leb_negative_mask) {
    dest[i++] |= (((xi_byte_t)value) & (~xi_leb_first_byte_mask)) | xi_leb_next_mask;
    value >>= xi_leb_negative_shift;
  } else {
    dest[i++] |= ((xi_byte_t)value);
    return i;
  }

  while (value >= xi_leb_next_mask) {
    if (i + 1 >= count) {
      return XI_VARINT_ENCODE_OUT_OF_MEMORY;
    }
    dest[i++] = ((xi_byte_t)value) | xi_leb_next_mask;
    value >>= xi_leb_next_shift;
  }
  dest[i++] = (xi_byte_t)value;
  return i;
}

size_t xi_encoding_varint_encode_uint16(uint16_t value, xi_byte_t *dest, const size_t count) {
  size_t i = 0;
  while (value >= xi_leb_next_mask) {
    if (i + 1 >= count) {
      return XI_VARINT_ENCODE_OUT_OF_MEMORY;
    }
    dest[i++] = ((xi_byte_t)value) | xi_leb_next_mask;
    value >>= xi_leb_next_shift;
  }
  dest[i++] = (xi_byte_t)value;
  return i;
}

size_t xi_encoding_varint_encode_int32(int32_t value, xi_byte_t *dest, const size_t count) {
  if (count < 1) {
    return XI_VARINT_ENCODE_OUT_OF_MEMORY;
  }

  if (value == INT_MIN) {
    dest[0] = xi_leb_negative_mask;
    return 1;
  }

  if (value < 0) {
    dest[0] = xi_leb_negative_mask;
    value *= -1;
  } else {
    dest[0] = 0;
  }

  size_t i = 0;
  if (value >= xi_leb_negative_mask) {
    dest[i++] |= (((xi_byte_t)value) & (~xi_leb_first_byte_mask)) | xi_leb_next_mask;
    value >>= xi_leb_negative_shift;
  } else {
    dest[i++] |= ((xi_byte_t)value);
    return i;
  }

  while (value >= xi_leb_next_mask) {
    if (i + 1 >= count) {
      return XI_VARINT_ENCODE_OUT_OF_MEMORY;
    }
    dest[i++] = ((xi_byte_t)value) | xi_leb_next_mask;
    value >>= xi_leb_next_shift;
  }
  dest[i++] = (xi_byte_t)value;
  return i;
}

size_t xi_encoding_varint_encode_uint32(uint32_t value, xi_byte_t *dest, const size_t count) {
  size_t i = 0;
  while (value >= xi_leb_next_mask) {
    if (i + 1 >= count) {
      return XI_VARINT_ENCODE_OUT_OF_MEMORY;
    }
    dest[i++] = ((xi_byte_t)value) | xi_leb_next_mask;
    value >>= xi_leb_next_shift;
  }
  dest[i++] = (xi_byte_t)value;
  return i;
}

size_t xi_encoding_varint_encode_int64(int64_t value, xi_byte_t *dest, const size_t count) {
  if (count < 1) {
    return XI_VARINT_ENCODE_OUT_OF_MEMORY;
  }

  if (value == LLONG_MIN) {
    dest[0] = xi_leb_negative_mask;
    return 1;
  }

  if (value < 0) {
    dest[0] = xi_leb_negative_mask;
    value *= -1;
  } else {
    dest[0] = 0;
  }

  size_t i = 0;
  if (value >= xi_leb_negative_mask) {
    dest[i++] |= (((xi_byte_t)value) & (~xi_leb_first_byte_mask)) | xi_leb_next_mask;
    value >>= xi_leb_negative_shift;
  } else {
    dest[i++] |= ((xi_byte_t)value);
    return i;
  }

  while (value >= xi_leb_next_mask) {
    if (i + 1 >= count) {
      return XI_VARINT_ENCODE_OUT_OF_MEMORY;
    }
    dest[i++] = ((xi_byte_t)value) | xi_leb_next_mask;
    value >>= xi_leb_next_shift;
  }
  dest[i++] = (xi_byte_t)value;
  return i;
}

size_t xi_encoding_varint_encode_uint64(uint64_t value, xi_byte_t *dest, const size_t count) {
  size_t i = 0;
  while (value >= xi_leb_next_mask) {
    if (i + 1 >= count) {
      return XI_VARINT_ENCODE_OUT_OF_MEMORY;
    }
    dest[i++] = ((xi_byte_t)value) | xi_leb_next_mask;
    value >>= xi_leb_next_shift;
  }
  dest[i++] = (xi_byte_t)value;
  return i;
}

int xi_encoding_varint_has_successor(xi_byte_t current) {
  if ((current & xi_leb_next_mask) == 0) {
    return (0 != 0);
  } else {
    return (0 == 0);
  }
}
