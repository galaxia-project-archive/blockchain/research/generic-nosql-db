/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Database/TableHandle.hpp"

#include <utility>

#include "Xi/Database/IDatabase.hpp"

Xi::Database::TableHandle::TableHandle(Xi::Database::WeakITable table,
                                       Xi::Database::WeakIDatabase database)
    : m_table{table}, m_database{database} {
  if (auto _table = m_table.lock()) {
    m_prefix = _table->prefix();
  }
}

Xi::Database::TableLock Xi::Database::TableHandle::lock() {
  auto db = m_database.lock();
  if (db) {
    const auto acquired = db->acquireResourceLock();
    if (acquired) {
      auto ref = m_table.lock();
      if (!ref) {
        ref = db->renewHandle(m_prefix).lock();
        if (!ref) {
          db->releaseResource();
        } else {
          return TableLock{ref, db};
        }

      } else {
        return TableLock{ref, db};
      }
    }
  }
  return TableLock{SharedITable{}, SharedIDatabase{}};
}
