/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Database/IDatabase.hpp"

#include <utility>
#include <thread>
#include <chrono>

#define XI_LOG_VERBOSE
#include <Xi/Log/Log.hpp>

XI_LOGGER("Xi/Database")

namespace Xi {
namespace Database {

IDatabase::IDatabase() {
  XI_VERBOSE("Constructed");
}

IDatabase::~IDatabase() {
}

IDatabase::State IDatabase::currentState() const {
  return m_state.load(std::memory_order_consume);
}

bool IDatabase::initialize() {
  try {
    auto state = currentState();
    XI_RETURN_EC_IF(state == State::Idle || state == State::Processing, true);

    using clock = std::chrono::high_resolution_clock;
    const auto start = clock::now();
    do {
      state = currentState();
      std::this_thread::sleep_for(std::chrono::milliseconds{50});
      XI_RETURN_EC_IF(clock::now() - start > std::chrono::milliseconds{400}, false);
    } while (state != State::Closed);

    XI_RETURN_EC_IF_NOT(m_state.compare_exchange_strong(state, State::Initializing), false);
    state = State::Initializing;
    if (!doInitialize()) {
      m_state.store(State::Closed, std::memory_order_release);
      return false;
    }

    m_state.store(State::Idle, std::memory_order_release);
    return true;
  } catch (std::exception& e) {
    m_state.store(State::Closed, std::memory_order_release);
    XI_FATAL("Initialization threw: {}", e.what());
    return false;
  }
}

bool IDatabase::shutdown(std::chrono::milliseconds timeout) {
  try {
    auto state = currentState();
    XI_RETURN_EC_IF(state == State::Closed, true);
    if (!m_state.compare_exchange_strong(state, State::ShuttingDown)) {
      XI_RETURN_EC_IF(state == State::Closed, true);
      return false;
    }

    using clock = std::chrono::high_resolution_clock;
    const auto start = clock::now();
    auto currentLocks = m_resourceLocks.load();
    while (currentLocks > 0) {
      std::this_thread::sleep_for(std::chrono::milliseconds{50});
      XI_RETURN_EC_IF(currentState() == State::Closed, true);
      XI_RETURN_EC_IF(clock::now() - start > timeout, false);
      currentLocks = m_resourceLocks.load();
    }
    return executeShutdown();
  } catch (std::exception& e) {
    return false;
    XI_FATAL("Shutdown threw: {}", e.what());
  }
}

bool IDatabase::acquireResourceLock() {
  auto state = currentState();
  XI_RETURN_EC_IF(state != State::Idle && state != State::Processing, false);
  [[maybe_unused]] auto currentLocks = m_resourceLocks += 1;
  XI_VERBOSE("Current table locks: {}", currentLocks);
  m_state.compare_exchange_strong(state, State::Processing);
  return true;
}

void IDatabase::releaseResource() {
  auto state = currentState();
  const auto currentLocks = --m_resourceLocks;
  if (currentLocks == 0) {
    if (state == State::ShuttingDown) {
      if (!executeShutdown()) {
        XI_FATAL("Pending shutdown failed.");
      }
    } else {
      m_state.compare_exchange_strong(state, State::Idle);
    }
  }
  XI_VERBOSE("Current table locks: {}", currentLocks);
}

uint16_t IDatabase::currentResourceLocks() const {
  return m_resourceLocks.load(std::memory_order_consume);
}

bool IDatabase::releaseTable(SharedITable table) {
  return doReleaseTable(*table);
}

bool IDatabase::executeShutdown() {
  XI_CONCURRENT_RLOCK(m_shutdownLock);
  auto state = currentState();
  XI_RETURN_EC_IF(state == State::Closed, true);
  XI_RETURN_EC_IF_NOT(state == State::ShuttingDown, false);

  for (auto& table : m_tables) {
    if (auto ok = releaseTable(table.second); !ok) {
      XI_ERROR("Failed to release table, shutdown may stall.");
    }
  }
  m_tables.clear();

  XI_RETURN_EC_IF_NOT(doShutdown(), false);
  m_state.store(State::Closed, std::memory_order_release);
  return true;
}

TableHandle IDatabase::requireTable(const TablePrefix& prefix) {
  return TableHandle{renewHandle(prefix), shared_from_this()};
}

WeakITable IDatabase::renewHandle(const TablePrefix& prefix) {
  XI_CONCURRENT_LOCK_PREPARE_WRITE(m_tablesGuard);
  auto search = m_tables.find(prefix);
  if (search != m_tables.end()) {
    return search->second;
  } else {
    auto table = doRequireTable(prefix);
    if (!table) {
      XI_WARN(
          "Required table was built at runtime, all tables should be created and referenced on "
          "startup using the "
          "Schema class.");
      return WeakITable{};
      ;
    } else {
      XI_CONCURRENT_LOCK_ACQUIRE_WRITE(m_tablesGuard);
      m_tables[prefix] = std::move(table);
      return m_tables[prefix];
    }
  }
}

}  // namespace Database
}  // namespace Xi
