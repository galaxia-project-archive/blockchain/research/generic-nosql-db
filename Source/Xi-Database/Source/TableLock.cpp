/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Database/TableLock.hpp"

#include <Xi/Log/Log.hpp>

#include "Xi/Database/IDatabase.hpp"

XI_LOGGER("Database/TableLock")

Xi::Database::TableLock::TableLock(Xi::Database::SharedITable ref, SharedIDatabase db)
    : m_ref{ref}, m_database{db}, m_moved{false} {
}

Xi::Database::TableLock::TableLock(Xi::Database::TableLock &&rhs)
    : m_ref{std::move(rhs.m_ref)}, m_database{std::move(rhs.m_database)}, m_moved{false} {
  rhs.m_moved = true;
}

Xi::Database::TableLock &Xi::Database::TableLock::operator=(Xi::Database::TableLock &&rhs) {
  release();
  m_ref = std::move(rhs.m_ref);
  m_database = std::move(rhs.m_database);
  rhs.m_moved = true;
  m_moved = false;
  return *this;
}

Xi::Database::TableLock::~TableLock() {
  release();
}

bool Xi::Database::TableLock::operator!() const {
  return m_ref.get() == nullptr;
}

Xi::Database::TableLock::operator bool() const {
  return m_ref.get() != nullptr;
}

Xi::Database::SharedITable Xi::Database::TableLock::operator->() {
  return m_ref;
}

void Xi::Database::TableLock::release() {
  if (m_ref && !m_moved) {
    auto database = m_database.lock();
    if (!database) {
      XI_FATAL("Resource lock could not be freed, database lock returned null.");
    } else {
      database->releaseResource();
    }
    m_moved = true;
  }
}
