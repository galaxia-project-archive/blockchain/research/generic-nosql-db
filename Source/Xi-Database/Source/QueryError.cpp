/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Database/QueryError.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Database, Query)
XI_ERROR_CODE_DESC(Success, "success")
XI_ERROR_CODE_DESC(NotFound, "nout found")
XI_ERROR_CODE_DESC(AlreadyExists, "already exists")
XI_ERROR_CODE_DESC(SerializationError, "serialization failed")
XI_ERROR_CODE_DESC(Busy, "database busy")
XI_ERROR_CODE_DESC(OutOfMemory, "ram or hard drive space exhausted")
XI_ERROR_CODE_DESC(InternalError, "internal error")
XI_ERROR_CODE_CATEGORY_END()
