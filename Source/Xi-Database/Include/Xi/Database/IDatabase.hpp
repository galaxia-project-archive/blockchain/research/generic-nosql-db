/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <atomic>
#include <memory>
#include <chrono>
#include <unordered_map>

#include <Xi/Concurrent/RecursiveLock.h>
#include <Xi/Concurrent/ReadersWriterLock.h>

#include "Xi/Database/ITable.hpp"
#include "Xi/Database/Table.hpp"

namespace Xi {
namespace Database {

/*!
 *
 * \brief The IDatabase class encapsulates a minimum interface to provide implementation independent
 * facilities.
 *
 * \attention NEVER create a none shared pointer from any database implementation. The IDatabase
 * class creates new shared pointer from itself for memory management purposes. You may want to make
 * the implementaton constructor private and provide a static method to create a shared pointer.
 *
 */
class IDatabase : public std::enable_shared_from_this<IDatabase> {
 protected:
  explicit IDatabase();

 public:
  enum struct State {
    Closed,
    Initializing,
    Idle,
    Processing,
    ShuttingDown,
  };

 private:
  /*!
   *
   * \brief requireTable tries to get a table implementation for the given table prefix.
   *
   * This internall queries a table implementation and allocates a memory holder for it. If none
   * could be allocated a nullptr is returned.
   *
   */
  [[nodiscard]] TableHandle requireTable(const TablePrefix& prefix);
  [[nodiscard]] WeakITable renewHandle(const TablePrefix& prefix);
  friend TableHandle;

 public:
  virtual ~IDatabase();

  /*!
   *
   * \brief initialize Heats up the underlying database implementation.
   * \return true on success othwerise false.
   *
   */
  [[nodiscard]] bool initialize();

  [[nodiscard]] bool shutdown(std::chrono::milliseconds timeout = std::chrono::seconds{5});

  [[nodiscard]] State currentState() const;

  /*!
   *
   * \brief getTable Queries a table implementation and wraps it with typesafe facilities.
   * \return A typesafe table wrapper, that may point to a null pointer.
   *
   */
  template <typename _TableTraitT>[[nodiscard]] std::shared_ptr<Table<_TableTraitT>> getTable() {
    return std::shared_ptr<Table<_TableTraitT>>{
        new Table<_TableTraitT>{requireTable(_TableTraitT::prefix())}};
  }

  // -------------------------------------------------------------------------------------------------------
  // Interface
 protected:
  /*!
   *
   * \brief doInititalize NoneVirtualInterface pattern, called from initialize.
   * \return true on success, otherwise false
   *
   */
  [[nodiscard]] virtual bool doInitialize() = 0;

  [[nodiscard]] virtual bool doShutdown() = 0;

  /*!
   *
   * \brief doRequireTable Called whenever a table is requested.
   * \param prefix The table prefix.
   * \return nullptr on failure, otherwise a database implementation based table.
   *
   * The IDatabase class takes care of the object memory lifetime. But guarantees the following to
   * the database implementation.
   *
   *  - Before the table gets release, releaseTable is called to the implementation passing the
   * Table.
   *  - Whenever a table is passed to the implementation, the table was allocated by the
   * implementation as well.
   *  - A table with the same prefix is never required again until releaseTable was called before.
   *
   */
  [[nodiscard]] virtual UniqueITable doRequireTable(const TablePrefix& prefix) = 0;

  /*!
   *
   * \brief doReleaseTable is called before shutdown, for each table referenced.
   * \return true on success, otherwise false
   *
   */
  [[nodiscard]] virtual bool doReleaseTable(ITable& table) = 0;

  // --------------------------------------------------------------------------------- Interactions
  // for Implementation
 protected:
  // -------------------------------------------------------------------------------------------
  // Resource Acquisition
 private:
  friend class TableHandle;
  friend class TableLock;

  /*!
   *
   * \brief acquireResourceLock is called from resource locks to indent that currently data is
   * processed. \return true if the resource can be locked, otherwise false.
   *
   */
  bool acquireResourceLock();

  /*!
   *
   * \brief releaseResouce is called from a resource lock once it is released, indicating that the
   * database may release resources.
   *
   */
  void releaseResource();

  /*!
   *
   * \brief currentResourceLocks queries the count of locks currently referencing resources.
   * \return zero if no resource is currently used otherwise the number of references using
   * resources.
   *
   */
  uint16_t currentResourceLocks() const;

 private:
  [[nodiscard]] bool releaseTable(SharedITable table);
  [[nodiscard]] bool executeShutdown();

 private:
  std::atomic<State> m_state{State::Closed};
  std::atomic_uint16_t m_resourceLocks{0};
  Concurrent::ReadersWriterLock m_tablesGuard{};
  Concurrent::RecursiveLock m_shutdownLock;
  std::unordered_map<TablePrefix, SharedITable> m_tables{};
};

}  // namespace Database
}  // namespace Xi
