/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <Xi/Stream/IInputStream.hpp>
#include <Xi/Stream/IOutputStream.hpp>
#include <Xi/Serialization/IInputSerializer.hpp>
#include <Xi/Serialization/IOutputSerializer.hpp>

#include "Xi/Database/TablePrefix.hpp"
#include "Xi/Database/IIterator.hpp"
#include "Xi/Database/QueryError.hpp"

namespace Xi {
namespace Database {

class ITable {
 protected:
  explicit ITable(const TablePrefix& prefix);

 public:
  virtual ~ITable() = default;

  const TablePrefix& prefix() const;

 protected:
  /*!
   *
   * \brief createInputSerailizer constructs a serializer applicable for the underlying database
   * implementation. \param input Table allocated storage to read from. \return a newly generate
   * input serializer.
   *
   */
  [[nodiscard]] virtual Serialization::UniqueIInputSerializer createKeyInputSerailizer(
      Stream::IInputStream& input) const = 0;

  /*!
   *
   * \brief createOutputSerializer constructs a serializer applicable for the underlying database
   * implementation. \param output Table allocated storage to write to. \return a newly generate
   * output serializer.
   *
   */
  [[nodiscard]] virtual Serialization::UniqueIOutputSerializer createKeyOutputSerailizer(
      Stream::IOutputStream& output) const = 0;

  /*!
   *
   * \brief createInputSerailizer constructs a serializer applicable for the underlying database
   * implementation. \param input Table allocated storage to read from. \return a newly generate
   * input serializer.
   *
   */
  [[nodiscard]] virtual Serialization::UniqueIInputSerializer createValueInputSerailizer(
      Stream::IInputStream& input) const = 0;

  /*!
   *
   * \brief createOutputSerializer constructs a serializer applicable for the underlying database
   * implementation. \param output Table allocated storage to write to. \return a newly generate
   * output serializer.
   *
   */
  [[nodiscard]] virtual Serialization::UniqueIOutputSerializer createValueOutputSerailizer(
      Stream::IOutputStream& output) const = 0;

  [[nodiscard]] virtual QueryError get(ConstByteSpan key, Stream::IOutputStream& valueOutput) = 0;
  [[nodiscard]] virtual QueryError put(ConstByteSpan key, ConstByteSpan value) = 0;
  [[nodiscard]] virtual QueryError del(ConstByteSpan key) = 0;
  [[nodiscard]] virtual QueryError del(ConstByteSpan range, ConstByteSpan exclusiveEnd) = 0;
  [[nodiscard]] virtual UniqueIIterator range(ConstByteSpan start) = 0;

 private:
  template <typename> friend struct TableSchema;
  template <typename _TableTraitT> friend class Table;

 private:
  TablePrefix m_prefix;
};

XI_DECLARE_SMART_POINTER(ITable)

}  // namespace Database
}  // namespace Xi
