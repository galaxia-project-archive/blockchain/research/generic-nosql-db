/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <functional>

#include <Xi/Byte.hh>

namespace Xi {
namespace Database {

struct TablePrefix {
  using id_type = uint16_t;
  static inline constexpr size_t bytes() {
    return sizeof(id_type);
  }

  explicit TablePrefix();
  explicit TablePrefix(id_type id);

  id_type id() const;
  ConstByteSpan raw() const;

  bool operator==(const TablePrefix& rhs) const;
  bool operator!=(const TablePrefix& rhs) const;

 private:
  id_type m_id;
};

}  // namespace Database
}  // namespace Xi

namespace std {

template <> struct hash<Xi::Database::TablePrefix> {
  size_t operator()(const Xi::Database::TablePrefix& prefix) const;
};

}  // namespace std
