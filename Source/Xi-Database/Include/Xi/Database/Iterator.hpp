/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <utility>
#include <cstring>
#include <algorithm>

#include <Xi/Exceptions.hpp>
#include <Xi/Result.h>

#include "Xi/Database/IIterator.hpp"
#include "Xi/Database/TableTrait.hpp"
#include "Xi/Database/TableLock.hpp"

namespace Xi {
namespace Database {

template <typename _TableTraitT> class Iterator final {
 public:
  using Schema = TableSchema<_TableTraitT>;

 private:
  UniqueIIterator m_iterator;
  mutable TableLock m_lock;

  XI_DELETE_COPY(Iterator);
  XI_DELETE_MOVE(Iterator);

 public:
  explicit Iterator(UniqueIIterator iterator, TableLock lock)
      : m_iterator{std::move(iterator)}, m_lock{std::move(lock)} {
    exceptional_if<NullArgumentError>(m_iterator.get() == nullptr);
    exceptional_if_not<NullArgumentError>(m_lock);
  }

  ~Iterator() {
    m_iterator.reset();
    m_lock.release();
  }

  [[nodiscard]] bool isEnd() const {
    return this->m_iterator->isEnd();
  }
  [[nodiscard]] QueryError error() const {
    return this->m_iterator->error();
  }
  void advance() {
    this->m_iterator->advance();
  }

  [[nodiscard]] bool operator==(const Iterator& rhs) const {
    XI_RETURN_EC_IF(this->isEnd(), rhs.isEnd());
    XI_RETURN_EC_IF(rhs.isEnd(), false);
    const auto thisKey = this->m_iterator->key();
    const auto rhsKey = rhs.m_iterator->key();
    XI_RETURN_EC_IF_NOT(thisKey.size() == rhsKey.size(), false);
    return std::memcmp(thisKey.data(), rhsKey.data(), thisKey.size()) == 0;
  }
  [[nodiscard]] bool operator!=(const Iterator& rhs) const {
    return !(*this == rhs);
  }

  [[nodiscard]] QueryError seek(const typename Schema::key_type key) {
    typename Schema::key_storage keyBuffer;
    XI_RETURN_EC_IF_NOT(Schema::serializeKey(m_lock, key, keyBuffer),
                        QueryError::SerializationError);
    m_iterator->seek(keyBuffer);
    return QueryError::Success;
  }

  Result<typename Schema::key_type> key() const {
    typename Schema::key_type reval{};
    XI_RETURN_EC_IF_NOT(Schema::deserializeKey(m_lock, this->m_iterator->key(), reval),
                        failure(QueryError::SerializationError));
    return success(std::move(reval));
  }

  Result<typename Schema::value_type> value() const {
    typename Schema::value_type reval{};
    XI_RETURN_EC_IF_NOT(Schema::deserializeValue(m_lock, this->m_iterator->value(), reval),
                        failure(QueryError::SerializationError));
    return success(std::move(reval));
  }
};

}  // namespace Database
}  // namespace Xi
