/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <type_traits>
#include <utility>

#include <Xi/Exceptions.hpp>
#include <Xi/Byte.hh>
#include <Xi/Stream/InMemoryStreams.hpp>
#include <Xi/Serialization/Serialization.hpp>

#include "Xi/Database/TablePrefix.hpp"
#include "Xi/Database/EntityTrait.hpp"
#include "Xi/Database/TableLock.hpp"
#include "Xi/Database/ITable.hpp"

namespace Xi {
namespace Database {
namespace Impl {
struct TableTraitTag {};
}  // namespace Impl

template <uint16_t _PrefixV, typename _KeyEntityT, typename _ValueEntityT>
struct TableTrait : Impl::TableTraitTag {
  static inline constexpr uint16_t prefixId() {
    return _PrefixV;
  }
  static inline TablePrefix prefix() {
    return TablePrefix{prefixId()};
  }

  using key_trait = _KeyEntityT;
  using key_type = typename key_trait::entity_type;
  using key_storage =
      std::conditional_t<key_trait::isFixedSize(),
                         ByteArray<TablePrefix::bytes() + key_trait::fixedSize()>, ByteVector>;

  using value_trait = _ValueEntityT;
  using value_type = typename value_trait::entity_type;
  using value_storage = std::conditional_t<value_trait::isFixedSize(),
                                           ByteArray<value_trait::fixedSize()>, ByteVector>;

  using key_value_storage =
      std::conditional_t<key_trait::isFixedSize() && value_trait::isFixedSize(),
                         ByteArray<TablePrefix::bytes() + key_trait::fixedSize()>, ByteVector>;

  static inline constexpr size_t minimumKeyValueSize() {
    if constexpr (key_trait::isFixedSize() && value_trait::isFixedSize()) {
      return TablePrefix::bytes() + key_trait::fixedSize() + value_trait::fixedSize();
    } else if constexpr (key_trait::isFixedSize()) {
      return TablePrefix::bytes() + key_trait::fixedSize();
    } else if constexpr (value_trait::isFixedSize()) {
      return TablePrefix::bytes() + value_trait::fixedSize();
    } else {
      return TablePrefix::bytes();
    }
  }
};

template <typename _TableTraitT> struct TableSchema final {
  static_assert(std::is_base_of_v<Impl::TableTraitTag, _TableTraitT>,
                "tables may only be constructed from table traits.");

  using table_trait_type = _TableTraitT;
  static inline constexpr uint16_t prefixId() {
    return table_trait_type::prefixId();
  }
  static inline TablePrefix prefix() {
    return table_trait_type::prefix();
  }

  using key_type = typename table_trait_type::key_type;
  using key_trait = typename table_trait_type::key_trait;
  using key_storage = typename table_trait_type::key_storage;

  using value_type = typename table_trait_type::value_type;
  using value_trait = typename table_trait_type::value_trait;
  using value_storage = typename table_trait_type::value_storage;

  using key_value_storage = typename table_trait_type::key_value_storage;
  using pair_type = std::pair<key_type, value_type>;

  [[nodiscard]] static bool serializeKey(TableLock& handle, const key_type& key, key_storage& out) {
    auto keyStream = Stream::asOutputStream(out);
    XI_RETURN_EC_IF_NOT(keyStream, false);
    XI_RETURN_EC_IF_NOT(Stream::writeStrict(*keyStream, handle->prefix().raw()), false);
    auto keySerializer = handle->createKeyOutputSerailizer(*keyStream);
    XI_RETURN_EC_IF_NOT(keySerializer, false);
    XI_RETURN_EC_IF_NOT(Serialization::serialize(const_cast<key_type&>(key), *keySerializer),
                        false);
    return true;
  }

  [[nodiscard]] static bool deserializeKey(TableLock& handle, ConstByteSpan input, key_type& out) {
    auto keyStream = Stream::asInputStream(input);
    XI_RETURN_EC_IF_NOT(keyStream, false);
    auto keySerializer = handle->createKeyInputSerailizer(*keyStream);
    XI_RETURN_EC_IF_NOT(keySerializer, false);
    XI_RETURN_EC_IF_NOT(Serialization::serialize(out, *keySerializer), false);
    return true;
  }

  [[nodiscard]] static bool serializeValue(TableLock& handle, const value_type& value,
                                           value_storage& out) {
    auto valueStream = Stream::asOutputStream(out);
    XI_RETURN_EC_IF_NOT(valueStream, false);
    auto valueSerializer = handle->createValueOutputSerailizer(*valueStream);
    XI_RETURN_EC_IF_NOT(valueSerializer, false);
    XI_RETURN_EC_IF_NOT(Serialization::serialize(const_cast<value_type&>(value), *valueSerializer),
                        false);
    return true;
  }

  [[nodiscard]] static bool deserializeValue(TableLock& handle, const value_storage& value,
                                             value_type& out) {
    auto valueStream = Stream::asInputStream(value);
    XI_RETURN_EC_IF_NOT(valueStream, false);
    auto valueSerializer = handle->createValueInputSerailizer(*valueStream);
    XI_RETURN_EC_IF_NOT(valueSerializer, false);
    XI_RETURN_EC_IF_NOT(Serialization::serialize(out, *valueSerializer), false);
    return true;
  }

  [[nodiscard]] static bool deserializeValue(TableLock& handle, ConstByteSpan input,
                                             value_type& out) {
    auto valueStream = Stream::asInputStream(input);
    XI_RETURN_EC_IF_NOT(valueStream, false);
    auto valueSerializer = handle->createValueInputSerailizer(*valueStream);
    XI_RETURN_EC_IF_NOT(valueSerializer, false);
    XI_RETURN_EC_IF_NOT(Serialization::serialize(out, *valueSerializer), false);
    return true;
  }
};

}  // namespace Database
}  // namespace Xi
