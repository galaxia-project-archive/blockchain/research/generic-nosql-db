/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/ErrorCode.hpp>

namespace Xi {
namespace Database {

XI_ERROR_CODE_BEGIN(Query)

/// Query performed as expected.
XI_ERROR_CODE_VALUE(Success, 0x0000)

/// A queried key was not found.
XI_ERROR_CODE_VALUE(NotFound, 0x0001)

/// Insertions could not override exisiting value.
XI_ERROR_CODE_VALUE(AlreadyExists, 0x0002)

/// Key/Value serialization failed.
XI_ERROR_CODE_VALUE(SerializationError, 0x0003)

/// Service is currently unavailable, try again later.
XI_ERROR_CODE_VALUE(Busy, 0x0004)

/// Service is currently unavailable, try again later.
XI_ERROR_CODE_VALUE(OutOfMemory, 0x0005)

/// An internal error occured.
XI_ERROR_CODE_VALUE(InternalError, 0x00FF)

XI_ERROR_CODE_END(Query, "Database/Query")

}  // namespace Database
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Database, Query)
