/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>

namespace Xi {
namespace Database {

XI_DECLARE_SMART_POINTER_CLASS(IDatabase)
XI_DECLARE_SMART_POINTER_CLASS(ITable)

class TableLock {
 private:
  /*!
   *
   * \brief Lock creates a new lock holding the table reference.
   * \param ref The locked table reference.
   * \param db The database manaing the references.
   *
   * \attention The construction assumes the database locked reference counter has already beed
   * increased.
   *
   */
  explicit TableLock(SharedITable ref, SharedIDatabase db);
  friend class TableHandle;

 public:
  XI_DELETE_COPY(TableLock);
  TableLock(TableLock&& rhs);
  TableLock& operator=(TableLock&& rhs);
  ~TableLock();

  operator bool() const;
  bool operator!() const;
  SharedITable operator->();

  void release();

 private:
  SharedITable m_ref;
  WeakIDatabase m_database;
  bool m_moved;
};

}  // namespace Database
}  // namespace Xi
