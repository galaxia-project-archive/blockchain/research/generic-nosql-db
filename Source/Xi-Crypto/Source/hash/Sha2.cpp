/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Crypto/Hash/Sha2.hh"

#include <Xi/Exceptions.hpp>

#include "Xi/Crypto/Hash/HashError.hpp"

XI_CRYPTO_HASH_DECLARE_HASH_IMPLEMENTATION(Xi::Crypto::Hash::Sha2::Hash224, 224);
XI_CRYPTO_HASH_DECLARE_HASH_IMPLEMENTATION(Xi::Crypto::Hash::Sha2::Hash256, 256);
XI_CRYPTO_HASH_DECLARE_HASH_IMPLEMENTATION(Xi::Crypto::Hash::Sha2::Hash384, 384);
XI_CRYPTO_HASH_DECLARE_HASH_IMPLEMENTATION(Xi::Crypto::Hash::Sha2::Hash512, 512);

Xi::Crypto::Hash::HashError Xi::Crypto::Hash::Sha2::compute(ConstByteSpan data, Hash224 &out) {
  XI_RETURN_EC_IF_NOT(
      xi_crypto_hash_sha2_224(data.data(), data.size(), out.data()) == XI_RETURN_CODE_SUCCESS,
      HashError::Internal);
  return HashError::Success;
}

Xi::Crypto::Hash::HashError Xi::Crypto::Hash::Sha2::compute(ConstByteSpan data, Hash256 &out) {
  XI_RETURN_EC_IF_NOT(
      xi_crypto_hash_sha2_256(data.data(), data.size(), out.data()) == XI_RETURN_CODE_SUCCESS,
      HashError::Internal);
  return HashError::Success;
}

Xi::Crypto::Hash::HashError Xi::Crypto::Hash::Sha2::compute(ConstByteSpan data, Hash384 &out) {
  XI_RETURN_EC_IF_NOT(
      xi_crypto_hash_sha2_384(data.data(), data.size(), out.data()) == XI_RETURN_CODE_SUCCESS,
      HashError::Internal);
  return HashError::Success;
}

Xi::Crypto::Hash::HashError Xi::Crypto::Hash::Sha2::compute(ConstByteSpan data, Hash512 &out) {
  XI_RETURN_EC_IF_NOT(
      xi_crypto_hash_sha2_512(data.data(), data.size(), out.data()) == XI_RETURN_CODE_SUCCESS,
      HashError::Internal);
  return HashError::Success;
}
