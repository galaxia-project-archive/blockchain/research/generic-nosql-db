/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Crypto/Hash/FastHash.hh"

#include <Xi/Exceptions.hpp>

Xi::Crypto::Hash::HashError Xi::Crypto::Hash::fastHash(Xi::ConstByteSpan data, Xi::ByteSpan out) {
  XI_RETURN_EC_IF(out.size() < XI_HASH_FAST_HASH_SIZE, HashError::OutOfMemory);
  if (const auto ec = xi_crypto_hash_fast_hash(data.data(), data.size(), out.data());
      ec != XI_RETURN_CODE_SUCCESS) {
    return HashError::Internal;
  } else {
    return HashError::Success;
  }
}
