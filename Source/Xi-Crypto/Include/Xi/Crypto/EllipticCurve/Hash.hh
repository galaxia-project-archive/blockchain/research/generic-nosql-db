/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include "Xi/Crypto/Hash/Keccak.hh"

#if defined(__cplusplus)
extern "C" {
#endif

#define XI_CRYPTO_ELLIPTIC_CURVE_HASH_SIZE XI_CRYPTO_HASH_KECCAK_HASH_SIZE

#define xi_crypto_elliptic_curve_hash xi_crypto_hash_keccak_256
#define xi_crypto_elliptic_curve_hash_state xi_crypto_hash_keccak_state
#define xi_crypto_elliptic_curve_hash_init xi_crypto_hash_keccak_init
#define xi_crypto_elliptic_curve_hash_update xi_crypto_hash_keccak_update
#define xi_crypto_elliptic_curve_hash_finish xi_crypto_hash_keccak_finish

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)

namespace Xi {
namespace Crypto {
namespace EllipticCurve {

using Hash = Hash::Keccak::Hash256;

}
}  // namespace Crypto
}  // namespace Xi

#endif
