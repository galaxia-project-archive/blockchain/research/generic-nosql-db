/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Result.h>
#include <Xi/Memory/SecureBlob.hpp>

#include "Xi/Crypto/EllipticCurve/Constants.hh"
#include "Xi/Crypto/EllipticCurve/Hash.hh"
#include "Xi/Crypto/EllipticCurve/Point.hpp"
#include "Xi/Crypto/EllipticCurve/SignatureError.hpp"

namespace Xi {
namespace Crypto {
namespace EllipticCurve {

struct Signature : Memory::EnableSecureBlobFromThis<Signature, signatureSize()> {
 public:
  static Result<Signature> sign(const Hash& messageHash, const Point& publicKey,
                                const Scalar& secretKey);
  static Result<Signature> sign(ConstByteSpan message, const Point& publicKey,
                                const Scalar& secretKey);

  using EnableSecureBlobFromThis::EnableSecureBlobFromThis;

  static const Signature Null;

  bool isValid() const;
  operator bool() const;
  bool operator!() const;

  [[nodiscard]] bool validate(const Hash& hashesMessage, const Point& publicKey) const;
  [[nodiscard]] bool validate(ConstByteSpan message, const Point& publicKey) const;

  const Byte* first() const;
  const Byte* second() const;

 private:
  friend struct RingSignature;

  Byte* mutableFirst();
  Byte* mutableSecond();
};

using SignatureVector = std::vector<Signature>;

}  // namespace EllipticCurve
}  // namespace Crypto
}  // namespace Xi
