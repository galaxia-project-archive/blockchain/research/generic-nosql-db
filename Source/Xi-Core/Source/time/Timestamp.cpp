/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Time/Timestamp.hpp"

#include <limits>

const Xi::Time::Timestamp Xi::Time::Timestamp::Null{Seconds{0}};

Xi::Time::Timestamp::Timestamp() : m_unix{Null.m_unix} {
}

Xi::Time::Timestamp::Timestamp(Seconds unix) {
  if (unix.count() < 0) {
    m_unix = 0;
  } else {
    m_unix = static_cast<uint64_t>(unix.count());
  }
}

Xi::Time::Timestamp::Timestamp(Xi::Time::Minutes mins) {
  if (mins.count() < 0 || mins.count() * 60 < mins.count()) {
    m_unix = 0;
  } else {
    m_unix = static_cast<uint64_t>(mins.count() * 60);
  }
}

Xi::Time::Timestamp::Timestamp(Xi::Time::Hours hours) {
  if (hours.count() < 0 || hours.count() * 3600 < hours.count()) {
    m_unix = 0;
  } else {
    m_unix = static_cast<uint64_t>(hours.count() * 3600);
  }
}

Xi::Time::Seconds Xi::Time::Timestamp::seconds() const {
  return Seconds{static_cast<int64_t>(m_unix)};
}

Xi::Time::Minutes Xi::Time::Timestamp::minutes() const {
  return std::chrono::duration_cast<Minutes>(seconds());
}

Xi::Time::Hours Xi::Time::Timestamp::hours() const {
  return std::chrono::duration_cast<Hours>(seconds());
}

uint64_t Xi::Time::Timestamp::count() const {
  return m_unix;
}

bool Xi::Time::Timestamp::isNull() const {
  return (*this) == Null;
}

bool Xi::Time::Timestamp::operator!() const {
  return isNull();
}

Xi::Time::Timestamp::operator bool() const {
  return !isNull();
}

bool Xi::Time::Timestamp::operator==(const Xi::Time::Timestamp &rhs) const {
  return m_unix == rhs.m_unix;
}

bool Xi::Time::Timestamp::operator!=(const Xi::Time::Timestamp &rhs) const {
  return m_unix != rhs.m_unix;
}

bool Xi::Time::Timestamp::operator<(const Xi::Time::Timestamp &rhs) const {
  return m_unix < rhs.m_unix;
}

bool Xi::Time::Timestamp::operator<=(const Xi::Time::Timestamp &rhs) const {
  return m_unix <= rhs.m_unix;
}

bool Xi::Time::Timestamp::operator>(const Xi::Time::Timestamp &rhs) const {
  return m_unix > rhs.m_unix;
}

bool Xi::Time::Timestamp::operator>=(const Xi::Time::Timestamp &rhs) const {
  return m_unix >= rhs.m_unix;
}

Xi::Time::Timestamp Xi::Time::Timestamp::operator+(const Xi::Time::Seconds _seconds) const {
  if (isNull() || seconds() + _seconds < seconds()) {
    return Null;
  } else {
    return Timestamp{seconds() + _seconds};
  }
}

Xi::Time::Timestamp Xi::Time::Timestamp::operator-(const Xi::Time::Seconds _seconds) const {
  if (isNull() || seconds() - _seconds > seconds()) {
    return Null;
  } else {
    return Timestamp{seconds() - _seconds};
  }
}

std::size_t std::hash<Xi::Time::Timestamp>::operator()(const Xi::Time::Timestamp value) const {
  return std::hash<uint64_t>{}(value.m_unix);
}
