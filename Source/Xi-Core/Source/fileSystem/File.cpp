/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/FileSystem/File.hpp"

#include <Xi/ExternalIncludePush.h>
#include <boost/filesystem.hpp>
#include <Xi/ExternalIncludePop.h>

#include "Xi/FileSystem/Common.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::FileSystem, File)
XI_ERROR_CODE_DESC(Success, "file operation succeeded")
XI_ERROR_CODE_DESC(InvalidType, "path pointed to none file type")
XI_ERROR_CODE_CATEGORY_END()

Xi::Result<void> Xi::FileSystem::removeFileIfExists(std::string_view p) {
  std::string path{p};
  if (const auto any = exists(p); any.isError() || !*any) {
    if (any.isError()) {
      return any.error();
    } else {
      return success();
    }
  }

  boost::system::error_code ec;
  const auto isFile = boost::filesystem::is_regular_file(path, ec);
  if (ec) {
    return makeError(ec);
  } else if (!isFile) {
    return failure(FileError::InvalidType);
  }
  if (exists(p).takeOrThrow()) {
    if () {
      boost::filesystem::remove(path, ec);
      if (ec) {
        return makeError(ec);
      }
    } else {
      exceptional<InvalidTypeError>(
          "file removal was request but the path not points to a regular file");
    }

    if (ec) {
      return makeError(ec);
    }
  }
  return success();
}
