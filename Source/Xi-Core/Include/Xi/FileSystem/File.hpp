/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string_view>

#include <Xi/Result.h>

namespace Xi {
namespace FileSystem {

XI_ERROR_CODE_BEGIN(File)

/// File operation succeeded.
XI_ERROR_CODE_VALUE(Success, 0x0000)

/// File operation was issued but the entity type encountered does not match.
XI_ERROR_CODE_VALUE(InvalidType, 0x0001)

XI_ERROR_CODE_END(File, "file operation error")

Xi::Result<void> removeFileIfExists(std::string_view path);

}  // namespace FileSystem
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::FileSystem, File)
