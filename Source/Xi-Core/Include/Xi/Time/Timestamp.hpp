/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <chrono>
#include <cinttypes>
#include <functional>

#include <Xi/Global.hh>

namespace Xi {
namespace Time {

using Seconds = std::chrono::seconds;
using Minutes = std::chrono::minutes;
using Hours = std::chrono::hours;

/*!
 * \brief The Timestamp struct guarantess to never return a negative duration. \attention Timestamps
 * constructed from a negative duration will be nullified. Further overflows from lower granualarity
 * durations will be nullified as well.
 */
struct Timestamp {
  static const Timestamp Null;

  /*!
   * \brief Timestamp default constructor nullifies the timestamp.
   */
  Timestamp();

  explicit Timestamp(Seconds unix);
  explicit Timestamp(Minutes mins);
  explicit Timestamp(Hours hours);
  XI_DEFAULT_COPY(Timestamp);
  ~Timestamp() = default;

  Seconds seconds() const;
  Minutes minutes() const;
  Hours hours() const;

  uint64_t count() const;

  bool isNull() const;
  operator bool() const;
  bool operator!() const;

  bool operator==(const Timestamp &rhs) const;
  bool operator!=(const Timestamp &rhs) const;
  bool operator<(const Timestamp &rhs) const;
  bool operator<=(const Timestamp &rhs) const;
  bool operator>(const Timestamp &rhs) const;
  bool operator>=(const Timestamp &rhs) const;

  Timestamp operator+(const Seconds seconds) const;
  Timestamp operator-(const Seconds seconds) const;

 private:
  friend std::hash<Timestamp>;

 private:
  uint64_t m_unix;
};

}  // namespace Time
}  // namespace Xi

namespace std {
template <> struct hash<Xi::Time::Timestamp> {
  size_t operator()(const Xi::Time::Timestamp value) const;
};
}  // namespace std

// Legacy Usage
namespace CryptoNote {

using Seconds = Xi::Time::Seconds;
using Minutes = Xi::Time::Minutes;
using Hours = Xi::Time::Hours;
using Timestamp = Xi::Time::Timestamp;

}  // namespace CryptoNote
