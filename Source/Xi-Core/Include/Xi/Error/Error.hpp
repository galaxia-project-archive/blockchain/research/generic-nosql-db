/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <system_error>

#include "Xi/Error/Code.hpp"
#include "Xi/Error/GlitchError.hpp"

namespace Xi {
namespace Error {

class [[nodiscard]] Error {
 public:
  /**/
  explicit Error();
  explicit Error(std::error_code ec);
  explicit Error(const Error& other);
  Error& operator=(const Error& other);
  explicit Error(Error && other);
  Error& operator=(Error&& other);

 private:
  std::error_code m_errorCode;
};
}  // namespace Error
}  // namespace Xi

// namespace fmt {
// template <> struct formatter<Xi::Error> {
//  template <typename ParseContext> constexpr auto parse(ParseContext &ctx) {
//    return ctx.begin();
//  }
//
//  template <typename FormatContext> auto format(const Xi::Error &err, FormatContext &ctx) {
//    if (err.isErrorCode()) {
//      return format_to(ctx.begin(), "[EC: {0} | '{1}'", err.errorCode().value(),
//                       err.errorCode().message());
//    } else if (err.isException()) {
//      return format_to(ctx.begin(), "[EX: '{}']", err.message());
//    } else if (err.isNotInitialized()) {
//      return format_to(ctx.begin(), "[NOT_INITIALIZED]");
//    } else {
//      return format_to(ctx.begin(), "[UNKNOWN]");
//    }
//  }
//};
//}  // namespace fmt
