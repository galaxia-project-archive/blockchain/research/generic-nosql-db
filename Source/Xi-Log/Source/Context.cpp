/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Log/Context.hpp"

Xi::Log::Context::Context()
    : m_timestamp{clock_type::now()},
      m_thread{std::this_thread::get_id()},
      m_level{Level::None},
      m_category{} {
}

Xi::Log::Context Xi::Log::Context::current(const Xi::Log::Level level,
                                           Xi::Log::WeakCategory category) {
  Context context{};
  context.m_level = level;
  context.m_category = category;
  return context;
}

Xi::Log::Context::time_point_type Xi::Log::Context::timestamp() const {
  return m_timestamp;
}

Xi::Log::Context::thread_id_type Xi::Log::Context::thread() const {
  return m_thread;
}

Xi::Log::Level Xi::Log::Context::level() {
  return m_level;
}

Xi::Log::WeakCategory Xi::Log::Context::category() const {
  return m_category;
}
