/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <chrono>
#include <thread>

#include <Xi/Global.hh>

#include "Xi/Log/Level.hpp"
#include "Xi/Log/Category.hpp"

namespace Xi {
namespace Log {

class Context final {
 public:
  using clock_type = std::chrono::high_resolution_clock;
  using time_point_type = clock_type::time_point;
  using thread_id_type = std::thread::id;

 public:
  static Context current(const Level level, WeakCategory category = WeakCategory{});

 private:
  Context();

 public:
  XI_DEFAULT_COPY(Context);
  XI_DEFAULT_MOVE(Context);
  ~Context() = default;

  time_point_type timestamp() const;
  thread_id_type thread() const;
  Level level();
  WeakCategory category() const;

 private:
  time_point_type m_timestamp;
  thread_id_type m_thread;
  Level m_level;
  WeakCategory m_category;
};

}  // namespace Log
}  // namespace Xi
