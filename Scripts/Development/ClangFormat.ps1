Get-ChildItem -Recurse -Path .\Source -Filter "*.hpp" | ForEach-Object { clang-format -i -style=file $_ }
Get-ChildItem -Recurse -Path .\Source -Filter "*.hh" | ForEach-Object { clang-format -i -style=file $_ }
Get-ChildItem -Recurse -Path .\Source -Filter "*.cpp" | ForEach-Object { clang-format -i -style=file $_ }
Get-ChildItem -Recurse -Path .\Source -Filter "*.c" | ForEach-Object { clang-format -i -style=file $_ }