﻿ # ============================================================================================== #
 #                                                                                                #
 #                                       Xi Blockchain                                            #
 #                                                                                                #
 # ---------------------------------------------------------------------------------------------- #
 # This file is part of the Galaxia Project - Xi Blockchain                                       #
 # ---------------------------------------------------------------------------------------------- #
 #                                                                                                #
 # Copyright 2018-2019 Galaxia Project Developers                                                 #
 #                                                                                                #
 # This program is free software: you can redistribute it and/or modify it under the terms of the #
 # GNU General Public License as published by the Free Software Foundation, either version 3 of   #
 # the License, or (at your option) any later version.                                            #
 #                                                                                                #
 # This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
 # without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
 # See the GNU General Public License for more details.                                           #
 #                                                                                                #
 # You should have received a copy of the GNU General Public License along with this program.     #
 # If not, see <https://www.gnu.org/licenses/>.                                                   #
 #                                                                                                #
 # ============================================================================================== #

if(XI_CMAKE_COMPILER_FLAG_CLEAR_CMAKE)
  return()
endif()
set(XI_CMAKE_COMPILER_FLAG_CLEAR_CMAKE ON)

macro(xi_compiler_flag_clear)
  cmake_parse_arguments(XI_COMPILER_CLEAR_FLAG "REGEX" "" "" ${ARGV})
  foreach(expr ${XI_COMPILER_CLEAR_FLAG_UNPARSED_ARGUMENTS})
    xi_status("Clearing compiler flag(s) '${expr}'")
    foreach(
      flag
        CMAKE_CXX_FLAGS
        CMAKE_CXX_FLAGS_DEBUG
        CMAKE_CXX_FLAGS_RELEASE
        CMAKE_CXX_FLAGS_RELWITHDEBINFO
        CMAKE_CXX_FLAGS_MINSIZEREL
        CMAKE_C_FLAGS
        CMAKE_C_FLAGS_DEBUG
        CMAKE_C_FLAGS_RELEASE
        CMAKE_C_FLAGS_RELWITHDEBINFO
        CMAKE_C_FLAGS_MINSIZEREL
    )
      if(XI_COMPILER_CLEAR_FLAG_REGEX)
        string(REGEX REPLACE ${expr} "" ${flag} ${${flag}})
      else()
        string(REPLACE ${expr} "" ${flag} ${${flag}})
      endif()
    endforeach()
  endforeach()
endmacro()
