﻿ # ============================================================================================== #
 #                                                                                                #
 #                                       Xi Blockchain                                            #
 #                                                                                                #
 # ---------------------------------------------------------------------------------------------- #
 # This file is part of the Galaxia Project - Xi Blockchain                                       #
 # ---------------------------------------------------------------------------------------------- #
 #                                                                                                #
 # Copyright 2018-2019 Galaxia Project Developers                                                 #
 #                                                                                                #
 # This program is free software: you can redistribute it and/or modify it under the terms of the #
 # GNU General Public License as published by the Free Software Foundation, either version 3 of   #
 # the License, or (at your option) any later version.                                            #
 #                                                                                                #
 # This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
 # without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
 # See the GNU General Public License for more details.                                           #
 #                                                                                                #
 # You should have received a copy of the GNU General Public License along with this program.     #
 # If not, see <https://www.gnu.org/licenses/>.                                                   #
 #                                                                                                #
 # ============================================================================================== #

if(XI_CMAKE_COMPILER_FLAG_ADD_CMAKE)
  return()
endif()
set(XI_CMAKE_COMPILER_FLAG_ADD_CMAKE ON)

include(CMakeParseArguments)

macro(xi_compiler_flag_add_impl name)
  foreach(flag ${ARGN})
    xi_debug("Add '${flag}' to ${name}")
    string(FIND "${${name}}" "${flag}" found)
    if(found LESS 0)
      set(${name} "${${name}} ${flag}")
    endif()
  endforeach()
endmacro()

macro(xi_compiler_flag_add)
  cmake_parse_arguments(XI_COMPILER_FLAG_ADD "" ""
    "COMMON;COMMON_RELEASE;COMMON_DEBUG;RELEASE;RELWITHDEBINFO;MINSIZEREL"
    ${ARGN}
  )

  xi_compiler_flag_add_impl(CMAKE_C_FLAGS ${XI_COMPILER_FLAG_ADD_COMMON})
  xi_compiler_flag_add_impl(CMAKE_CXX_FLAGS ${XI_COMPILER_FLAG_ADD_COMMON})

  xi_compiler_flag_add_impl(CMAKE_C_FLAGS_RELEASE ${XI_COMPILER_FLAG_ADD_COMMON_RELEASE})
  xi_compiler_flag_add_impl(CMAKE_CXX_FLAGS_RELEASE ${XI_COMPILER_FLAG_ADD_COMMON_RELEASE})
  xi_compiler_flag_add_impl(CMAKE_C_FLAGS_RELWITHDEBINFO ${XI_COMPILER_FLAG_ADD_COMMON_RELEASE})
  xi_compiler_flag_add_impl(CMAKE_CXX_FLAGS_RELWITHDEBINFO ${XI_COMPILER_FLAG_ADD_COMMON_RELEASE})
  xi_compiler_flag_add_impl(CMAKE_C_FLAGS_MINSIZEREL ${XI_COMPILER_FLAG_ADD_COMMON_RELEASE})
  xi_compiler_flag_add_impl(CMAKE_CXX_FLAGS_MINSIZEREL ${XI_COMPILER_FLAG_ADD_COMMON_RELEASE})

  xi_compiler_flag_add_impl(CMAKE_C_FLAGS_DEBUG ${XI_COMPILER_FLAG_ADD_COMMON_DEBUG})
  xi_compiler_flag_add_impl(CMAKE_CXX_FLAGS_DEBUG ${XI_COMPILER_FLAG_ADD_COMMON_DEBUG})

  foreach(alias DEBUG RELEASE RELWITHDEBINFO MINSIZEREL)
    xi_compiler_flag_add_impl("CMAKE_C_FLAGS_${alias}" ${XI_COMPILER_FLAG_ADD_${alias}})
    xi_compiler_flag_add_impl("CMAKE_CXX_FLAGS_${alias}" ${XI_COMPILER_FLAG_ADD_${alias}})
  endforeach()
endmacro()

macro(xi_linker_flag_add)
  cmake_parse_arguments(XI_COMPILER_FLAG_ADD "" ""
    "COMMON;COMMON_RELEASE;COMMON_DEBUG;RELEASE;RELWITHDEBINFO;MINSIZEREL;DEBUG"
    ${ARGN}
    )

  xi_compiler_flag_add_impl(CMAKE_EXE_LINKER_FLAGS ${XI_COMPILER_FLAG_ADD_COMMON})

  xi_compiler_flag_add_impl(CMAKE_EXE_LINKER_FLAGS_RELEASE ${XI_COMPILER_FLAG_ADD_COMMON_RELEASE})
  xi_compiler_flag_add_impl(CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO ${XI_COMPILER_FLAG_ADD_COMMON_RELEASE})
  xi_compiler_flag_add_impl(CMAKE_EXE_LINKER_FLAGS_MINSIZEREL ${XI_COMPILER_FLAG_ADD_COMMON_RELEASE})

  xi_compiler_flag_add_impl(CMAKE_EXE_LINKER_FLAGS_DEBUG ${XI_COMPILER_FLAG_ADD_COMMON_DEBUG})

  foreach(alias DEBUG RELEASE RELWITHDEBINFO MINSIZEREL)
    xi_compiler_flag_add_impl("CMAKE_EXE_LINKER_FLAGS_${alias}" ${XI_COMPILER_FLAG_ADD_${alias}})
  endforeach()
endmacro()
