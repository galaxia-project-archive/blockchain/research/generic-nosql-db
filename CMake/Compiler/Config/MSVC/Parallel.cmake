# ------------------------------------------------------------------------------------------------------------------- #
#                                                                                                                     #
#                                                   MIT Licenese                                                      #
#                                                                                                                     #
# ------------------------------------------------------------------------------------------------------------------- #
#                                                                                                                     #
# Copyright 2019 Michael Herwig <michael.herwig@hotmail.de>                                                           #
#                                                                                                                     #
# ------------------------------------------------------------------------------------------------------------------- #
#                                                                                                                     #
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated        #
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation the #
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to     #
# permit persons to whom the Software is furnished to do so, subject to the following conditions:                     #
#                                                                                                                     #
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of    #
# the Software.                                                                                                       #
#                                                                                                                     #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO    #
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE      #
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, #
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE      #
# SOFTWARE.                                                                                                           #
#                                                                                                                     #
# ------------------------------------------------------------------------------------------------------------------- #

if(XI_CMAKE_COMPILER_MSVC_PARALLEL_CMAKE)
  return()
endif()
set(XI_CMAKE_COMPILER_MSVC_PARALLEL_CMAKE ON)

option(XI_PARALLEL_BUILD OFF "enables parallel compiler execution")
set(XI_PARALLEL_BUILD_THREADS "-1" CACHE STRING "maximum number of threads to use for parallel compilation (<=0 -> max)")

xi_include(GLOBAL Compiler/Flag/Flag)

if(XI_PARALLEL_BUILD)
  if(XI_PARALLEL_BUILD_THREADS LESS_EQUAL "0")
    xi_status("Enabling parallel compilation with maximum threads.")
    xi_compiler_flag_add(COMMON "/MP")
  else()
    xi_status("Enabling parallel compilation with ${XI_PARALLEL_BUILD_THREADS} threads.")
    xi_compiler_flag_add(COMMON "/MP${XI_PARALLEL_BUILD_THREADS}")
  endif()
endif()
