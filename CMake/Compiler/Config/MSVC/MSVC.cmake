# ============================================================================================== #
#                                                                                                #
#                                       Xi Blockchain                                            #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Galaxia Project - Xi Blockchain                                       #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Galaxia Project Developers                                                 #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(XI_CMAKE_COMPILER_CONFIG_MSVC_CMAKE)
  return()
endif()
set(XI_CMAKE_COMPILER_CONFIG_MSVC_CMAKE ON)

xi_include(GLOBAL Common/Common)
xi_include(GLOBAL Compiler/Classify/Classify)
xi_include(GLOBAL Compiler/Flag/Flag)

if(NOT XI_MSVC)
  xi_fatal("MSVC compiler configuration assumes msvc usage.")
endif()

xi_include(Parallel)

xi_compiler_flag_clear(REGEX "/W[01234]")
xi_compiler_flag_clear("/WX" "/Wall" "/w")

xi_compiler_flag_add(COMMON "/W4" "/WX" "/std:c++17")
