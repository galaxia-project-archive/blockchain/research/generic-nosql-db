﻿ # ============================================================================================== #
 #                                                                                                #
 #                                       Xi Blockchain                                            #
 #                                                                                                #
 # ---------------------------------------------------------------------------------------------- #
 # This file is part of the Galaxia Project - Xi Blockchain                                       #
 # ---------------------------------------------------------------------------------------------- #
 #                                                                                                #
 # Copyright 2018-2019 Galaxia Project Developers                                                 #
 #                                                                                                #
 # This program is free software: you can redistribute it and/or modify it under the terms of the #
 # GNU General Public License as published by the Free Software Foundation, either version 3 of   #
 # the License, or (at your option) any later version.                                            #
 #                                                                                                #
 # This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
 # without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
 # See the GNU General Public License for more details.                                           #
 #                                                                                                #
 # You should have received a copy of the GNU General Public License along with this program.     #
 # If not, see <https://www.gnu.org/licenses/>.                                                   #
 #                                                                                                #
 # ============================================================================================== #

if(XI_CMAKE_COMPILER_CLASSIFY_ID_CMAKE)
  return()
endif()
set(XI_CMAKE_COMPILER_CLASSIFY_ID_CMAKE ON)

if(NOT CMAKE_C_COMPILER_ID STREQUAL CMAKE_CXX_COMPILER_ID)
  xi_fatal("C and CXX compiler do not match.")
endif()


set(
  XI_COMPILER_SUPPORTED_IDS
    MSVC
    GCC
    INTEL
    CLANG

  CACHE INTERNAL "" FORCE
)

foreach(compiler ${XI_COMPILER_SUPPORTED_ID})
  set(XI_${compiler} OFF CACHE INTERNAL "" FORCE)
endforeach()

if(CMAKE_C_COMPILER_ID STREQUAL "MSVC")
  set(XI_MSVC ON CACHE INTERNAL "" FORCE)
  set(XI_COMPILER_ID "MSVC" CACHE INTERNAL "" FORCE)
elseif(CMAKE_C_COMPILER_ID STREQUAL "GNU")
  set(XI_GCC ON CACHE INTERNAL "" FORCE)
  set(XI_COMPILER_ID "GCC" CACHE INTERNAL "" FORCE)
elseif(CMAKE_C_COMPILER_ID MATCHES "^(Apple)?Clang$")
  set(XI_CLANG ON CACHE INTERNAL "" FORCE)
  set(XI_COMPILER_ID "Clang" CACHE INTERNAL "" FORCE)
else()
  xi_fatal("Unsupported compiler id: ${CMAKE_C_COMPILER_ID}")
endif()
xi_debug("${XI_COMPILER_ID}")
