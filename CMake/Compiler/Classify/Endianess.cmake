 # ============================================================================================== #
 #                                                                                                #
 #                                       Xi Blockchain                                            #
 #                                                                                                #
 # ---------------------------------------------------------------------------------------------- #
 # This file is part of the Galaxia Project - Xi Blockchain                                       #
 # ---------------------------------------------------------------------------------------------- #
 #                                                                                                #
 # Copyright 2018-2019 Galaxia Project Developers                                                 #
 #                                                                                                #
 # This program is free software: you can redistribute it and/or modify it under the terms of the #
 # GNU General Public License as published by the Free Software Foundation, either version 3 of   #
 # the License, or (at your option) any later version.                                            #
 #                                                                                                #
 # This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
 # without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
 # See the GNU General Public License for more details.                                           #
 #                                                                                                #
 # You should have received a copy of the GNU General Public License along with this program.     #
 # If not, see <https://www.gnu.org/licenses/>.                                                   #
 #                                                                                                #
 # ============================================================================================== #

if(XI_CMAKE_COMPILER_CLASSIFY_ENDIANESS_CMAKE)
 return()
endif()
set(XI_CMAKE_COMPILER_CLASSIFY_ENDIANESS_CMAKE ON)

include(TestBigEndian)
test_big_endian(XI_COMPILER_BIG_ENDIAN)
if(NOT XI_COMPILER_BIG_ENDIAN)
  set(XI_COMPILER_LITTLE_ENDIAN TRUE CACHE INTERNAL "")
  set(XI_COMPILER_BIG_ENDIAN FALSE CACHE INTERNAL "")
  set(XI_COMPILER_ENDIANESS "LITTLE" CACHE INTERNAL "")
else()
  set(XI_COMPILER_LITTLE_ENDIAN FALSE CACHE INTERNAL "")
  set(XI_COMPILER_BIG_ENDIAN TRUE CACHE INTERNAL "")
  set(XI_COMPILER_ENDIANESS "BIG" CACHE INTERNAL "")
endif()