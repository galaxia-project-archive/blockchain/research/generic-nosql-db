# ============================================================================================== #
#                                                                                                #
#                                       Xi Blockchain                                            #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Galaxia Project - Xi Blockchain                                       #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Galaxia Project Developers                                                 #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(XI_CMAKE_COMPILER_CLASSIFY_RUNTIME_CMAKE)
  return()
endif()
set(XI_CMAKE_COMPILER_CLASSIFY_RUNTIME_CMAKE ON)

xi_include(Id)
xi_include(GLOBAL Compiler/Flag/Flag)

if(XI_MSVC)
  set(mt_found FALSE)
  set(md_found FALSE)

  foreach(flag ${XI_COMPILER_STATE_VARS})
    if(${${flag}} MATCHES "/MDd?")
      set(md_found TRUE)
    endif()
    if(${${flag}} MATCHES "/MTd?")
      set(mt_found TRUE)
    endif()
  endforeach()

  if(md_found AND mt_found)
    xi_fatal("Mixed runtime setup is not supported on windows.")
  elseif(md_found)
    set(XI_RT_DYNAMIC ON CACHE INTERNAL "")
    set(XI_RT_STATIC OFF CACHE INTERNAL "")
    set(XI_RT "DYNAMIC" CACHE INTERNAL "")
  elseif(mt_found)
    set(XI_RT_DYNAMIC OFF CACHE INTERNAL "")
    set(XI_RT_STATIC ON CACHE INTERNAL "")
    set(XI_RT "STATIC" CACHE INTERNAL "")
  else()
    xi_fatal("Unable to determine runtime linkage for MSVC.")
  endif()
endif()
