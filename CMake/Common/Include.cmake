﻿ # ============================================================================================== #
 #                                                                                                #
 #                                       Xi Blockchain                                            #
 #                                                                                                #
 # ---------------------------------------------------------------------------------------------- #
 # This file is part of the Galaxia Project - Xi Blockchain                                       #
 # ---------------------------------------------------------------------------------------------- #
 #                                                                                                #
 # Copyright 2018-2019 Galaxia Project Developers                                                 #
 #                                                                                                #
 # This program is free software: you can redistribute it and/or modify it under the terms of the #
 # GNU General Public License as published by the Free Software Foundation, either version 3 of   #
 # the License, or (at your option) any later version.                                            #
 #                                                                                                #
 # This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
 # without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
 # See the GNU General Public License for more details.                                           #
 #                                                                                                #
 # You should have received a copy of the GNU General Public License along with this program.     #
 # If not, see <https://www.gnu.org/licenses/>.                                                   #
 #                                                                                                #
 # ============================================================================================== #

if(XI_CMAKE_COMMON_INCLUDE_CMAKE)
  return()
endif()
set(XI_CMAKE_COMMON_INCLUDE_CMAKE ON)

include(CMakeParseArguments)

macro(xi_include)
  cmake_parse_arguments(
    XI_INCLUDE
      "LOCAL;GLOBAL"
      ""
      ""
    ${ARGN}
  )

  if(XI_INCLUDE_LOCAL)
    set(basepath "${CMAKE_CURRENT_LIST_DIR}")
  elseif(XI_INCLUDE_GLOBAL)
    set(basepath "${CMAKE_SOURCE_DIR}/CMake")
  else()
    set(basepath "${CMAKE_CURRENT_LIST_DIR}")
  endif()

  foreach(file ${XI_INCLUDE_UNPARSED_ARGUMENTS})
    include("${basepath}/${file}.cmake")
  endforeach()
endmacro()
