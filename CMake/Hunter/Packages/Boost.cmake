# ============================================================================================== #
#                                                                                                #
#                                       Xi Blockchain                                            #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Galaxia Project - Xi Blockchain                                       #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Galaxia Project Developers                                                 #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

set(
  XI_BOOST_REQUIRED_COMPONENTS
    context
    filesystem
    fiber
)

hunter_add_package(
  Boost

  COMPONENTS
    ${XI_BOOST_REQUIRED_COMPONENTS}
)

find_package(
  Boost

  REQUIRED COMPONENTS
    ${XI_BOOST_REQUIRED_COMPONENTS}
)

mark_as_advanced(BZip2_DIR)

set(Boost_VERSION ${Boost_VERSION} CACHE INTERNAL "Boost Version" FORCE)
mark_as_advanced(Boost_VERSION)
